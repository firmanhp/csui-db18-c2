# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals
from datetime import date

from django.db import models, connection
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.hashers import make_password
from .helpers import sql


class UserManager(BaseUserManager):
    use_in_migrations = True

    def create_user(self, username, password, role, **kwargs):
        if not username:
            raise ValueError('The given username must be set')

        with connection.cursor() as cursor:
            command = sql.create_insert_command(Pengguna._meta.db_table,
                                                username=username,
                                                password=make_password(password),
                                                role=role,
                                                **kwargs)
            cursor.execute(command)
            query_set = Pengguna.objects.raw("SELECT * FROM " + Pengguna._meta.db_table + " WHERE username = %s", [username])
            if len(list(query_set)) > 0:
                return query_set[0]
            else:
                raise Exception('Error when creating user')

    def create_general_user(self, username, password, role, **kwargs):
        kwargs.setdefault('is_superuser', False)
        return self.create_user(username, password, role, **kwargs)

    def create_superuser(self, username, password, **kwargs):
        kwargs.setdefault('is_superuser', True)
        return self.create_user(username, password, 'admin', **kwargs)

    def create_admin(self, username, password, **kwargs):
        pengguna = self.create_superuser(username, password, **kwargs)
        with connection.cursor() as cursor:
            command = sql.create_insert_command(Admin._meta.db_table,
                                                username=username)
            cursor.execute(command)
        return pengguna

    def create_donatur(self, username,
                       password,
                       nomor_identitas,
                       email,
                       nama,
                       npwp,
                       no_telp,
                       alamat):
        pengguna = self.create_general_user(username, password, 'donatur')
        email = self.normalize_email(email)
        with connection.cursor() as cursor:
            command = sql.create_insert_command(Donatur._meta.db_table,
                                                nomor_identitas=nomor_identitas,
                                                email=email,
                                                nama=nama,
                                                npwp=npwp,
                                                no_telp=no_telp,
                                                alamat=alamat,
                                                username=username)
            cursor.execute(command)
        return pengguna

    def create_donatur_individual_donor(self, username,
                                        password,
                                        nomor_identitas,
                                        email,
                                        nama,
                                        npwp,
                                        no_telp,
                                        alamat,
                                        nik):
        pengguna = self.create_donatur(username,
                                       password,
                                       nomor_identitas,
                                       email,
                                       nama,
                                       npwp,
                                       no_telp,
                                       alamat)
        with connection.cursor() as cursor:
            command = sql.create_insert_command(IndividualDonor._meta.db_table,
                                                nik=nik,
                                                nomor_identitas_donatur=nomor_identitas)
            cursor.execute(command)
        return pengguna

    def create_donatur_yayasan(self, username,
                               password,
                               nomor_identitas,
                               email,
                               nama,
                               npwp,
                               no_telp,
                               alamat,
                               no_sk_yayasan,
                               email_yayasan,
                               nama_yayasan,
                               no_telp_cp):
        pengguna = self.create_donatur(username,
                                       password,
                                       nomor_identitas,
                                       email,
                                       nama,
                                       npwp,
                                       no_telp,
                                       alamat)
        email_yayasan = self.normalize_email(email_yayasan)
        with connection.cursor() as cursor:
            command = sql.create_insert_command(Yayasan._meta.db_table,
                                                no_sk_yayasan=no_sk_yayasan,
                                                email=email_yayasan,
                                                nama=nama_yayasan,
                                                no_telp_cp=no_telp_cp,
                                                nomor_identitas_donatur=nomor_identitas)
            cursor.execute(command)
        return pengguna

    def create_mahasiswa(self, username,
                         password,
                         npm,
                         email,
                         nama,
                         no_telp,
                         alamat_tinggal,
                         alamat_domisili,
                         nama_bank,
                         no_rekening,
                         nama_pemilik):
        pengguna = self.create_general_user(username, password, 'mahasiswa')
        email = self.normalize_email(email)
        with connection.cursor() as cursor:
            command = sql.create_insert_command(Mahasiswa._meta.db_table,
                                                npm=npm,
                                                email=email,
                                                nama=nama,
                                                no_telp=no_telp,
                                                alamat_tinggal=alamat_tinggal,
                                                alamat_domisili=alamat_domisili,
                                                nama_bank=nama_bank,
                                                no_rekening=no_rekening,
                                                nama_pemilik=nama_pemilik,
                                                username=username)
            cursor.execute(command)
        return pengguna


class Pengguna(AbstractBaseUser, PermissionsMixin):

    objects = UserManager()

    username = models.CharField(primary_key=True, max_length=20)
    role = models.CharField(max_length=20)
    is_superuser = models.BooleanField(default=False)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    class Meta:
        db_table = 'pengguna'

    @property
    def is_admin(self):
        return self.role == 'admin'

    @property
    def is_donatur(self):
        return self.role == 'donatur'

    @property
    def is_mahasiswa(self):
        return self.role == 'mahasiswa'


class Admin(models.Model):
    username = models.ForeignKey('Pengguna', models.CASCADE, db_column='username', primary_key=True)

    class Meta:
        db_table = 'admin'
        managed = False

class Donatur(models.Model):
    nomor_identitas = models.CharField(primary_key=True, max_length=20)
    email = models.CharField(max_length=50)
    nama = models.CharField(max_length=50)
    npwp = models.CharField(max_length=20)
    no_telp = models.CharField(max_length=20, blank=True, null=True)
    alamat = models.CharField(max_length=50)

    username = models.ForeignKey('Pengguna', models.CASCADE, db_column='username')

    class Meta:
        db_table = 'donatur'
        managed = False


class IndividualDonor(models.Model):
    nik = models.CharField(primary_key=True, max_length=16)
    nomor_identitas_donatur = models.ForeignKey(Donatur, models.CASCADE, db_column='nomor_identitas_donatur')

    class Meta:
        db_table = 'individual_donor'
        managed = False


class Mahasiswa(models.Model):
    npm = models.CharField(primary_key=True, max_length=20)
    email = models.CharField(max_length=50)
    nama = models.CharField(max_length=50)
    no_telp = models.CharField(max_length=20, blank=True, null=True)
    alamat_tinggal = models.CharField(max_length=50)
    alamat_domisili = models.CharField(max_length=50)
    nama_bank = models.CharField(max_length=50)
    no_rekening = models.CharField(max_length=20)
    nama_pemilik = models.CharField(max_length=20)
    username = models.ForeignKey('Pengguna', models.CASCADE, db_column='username')

    class Meta:
        db_table = 'mahasiswa'
        managed = False


class Pembayaran(models.Model):
    urutan = models.IntegerField(primary_key=True)
    kode_skema_beasiswa = models.ForeignKey('SkemaBeasiswaAktif', models.CASCADE, db_column='kode_skema_beasiswa')
    no_urut_skema_beasiswa_aktif = models.IntegerField()
    npm = models.ForeignKey(Mahasiswa, models.CASCADE, db_column='npm')
    keterangan = models.CharField(max_length=50)
    tgl_bayar = models.DateField()
    nominal = models.IntegerField()

    class Meta:
        db_table = 'pembayaran'
        unique_together = (('urutan', 'kode_skema_beasiswa', 'no_urut_skema_beasiswa_aktif', 'npm'),)
        managed = False


class Pendaftaran(models.Model):
    no_urut = models.ForeignKey('SkemaBeasiswaAktif', models.CASCADE, db_column='no_urut', primary_key=True)
    kode_skema_beasiswa = models.IntegerField()
    npm = models.ForeignKey(Mahasiswa, models.CASCADE, db_column='npm')
    waktu_daftar = models.DateTimeField()
    status_daftar = models.CharField(max_length=20)
    status_terima = models.CharField(max_length=20)

    class Meta:
        db_table = 'pendaftaran'
        unique_together = (('no_urut', 'kode_skema_beasiswa', 'npm'),)
        managed = False


class Pengumuman(models.Model):
    tanggal = models.DateField(primary_key=True)
    no_urut_skema_beasiswa_aktif = models.ForeignKey('SkemaBeasiswaAktif', models.CASCADE, db_column='no_urut_skema_beasiswa_aktif')
    kode_skema_beasiswa = models.IntegerField()
    username = models.ForeignKey(Pengguna, models.CASCADE, db_column='username')
    judul = models.CharField(max_length=20)
    isi = models.CharField(max_length=255)

    class Meta:
        db_table = 'pengumuman'
        unique_together = (('tanggal', 'no_urut_skema_beasiswa_aktif', 'kode_skema_beasiswa', 'username'),)
        managed = False


class RiwayatAkademik(models.Model):
    no_urut = models.IntegerField(primary_key=True)
    npm = models.ForeignKey(Mahasiswa, models.CASCADE, db_column='npm')
    semester = models.CharField(max_length=1)
    tahun_ajaran = models.CharField(max_length=9)
    jumlah_sks = models.IntegerField()
    ips = models.FloatField()
    lampiran = models.CharField(max_length=50)

    class Meta:
        db_table = 'riwayat_akademik'
        unique_together = (('no_urut', 'npm'),)
        managed = False


class SkemaBeasiswa(models.Model):
    kode = models.IntegerField(primary_key=True)
    nama = models.CharField(max_length=50)
    jenis = models.CharField(max_length=20)
    deskripsi = models.CharField(max_length=50)
    nomor_identitas_donatur = models.ForeignKey(Donatur, models.CASCADE, db_column='nomor_identitas_donatur')

    class Meta:
        db_table = 'skema_beasiswa'
        managed = False


class SkemaBeasiswaAktif(models.Model):
    kode_skema_beasiswa = models.ForeignKey(SkemaBeasiswa, models.CASCADE, db_column='kode_skema_beasiswa', primary_key=True)
    no_urut = models.IntegerField()
    tgl_mulai_pendaftaran = models.DateField()
    tgl_tutup_pendaftaran = models.DateField()
    periode_penerimaan = models.CharField(max_length=50)
    status = models.CharField(max_length=20)
    jumlah_pendaftar = models.IntegerField()
    total_pembayaran = models.BigIntegerField()

    class Meta:
        db_table = 'skema_beasiswa_aktif'
        unique_together = (('kode_skema_beasiswa', 'no_urut'),)
        managed = False

    @property
    def is_registration_open(self):
        return self.tgl_mulai_pendaftaran <= date.today() <= self.tgl_tutup_pendaftaran


class SyaratBeasiswa(models.Model):
    kode_beasiswa = models.ForeignKey(SkemaBeasiswa, models.CASCADE, db_column='kode_beasiswa', primary_key=True)
    syarat = models.CharField(max_length=50)

    class Meta:
        db_table = 'syarat_beasiswa'
        unique_together = (('kode_beasiswa', 'syarat'),)
        managed = False


class TempatWawancara(models.Model):
    kode = models.IntegerField(primary_key=True)
    nama = models.CharField(max_length=50)
    lokasi = models.CharField(max_length=50)

    class Meta:
        db_table = 'tempat_wawancara'
        managed = False


class Wawancara(models.Model):
    no_urut_skema_beasiswa_aktif = models.IntegerField(primary_key=True)
    kode_skema_beasiswa = models.ForeignKey(SkemaBeasiswaAktif, models.CASCADE, db_column='kode_skema_beasiswa')
    jadwal = models.DateTimeField()
    kode_tempat_wawancara = models.ForeignKey(TempatWawancara, models.CASCADE, db_column='kode_tempat_wawancara')

    class Meta:
        db_table = 'wawancara'
        unique_together = (('no_urut_skema_beasiswa_aktif', 'kode_skema_beasiswa', 'jadwal'),)
        managed = False


class Yayasan(models.Model):
    no_sk_yayasan = models.CharField(primary_key=True, max_length=20)
    email = models.CharField(max_length=50)
    nama = models.CharField(max_length=50)
    no_telp_cp = models.CharField(max_length=20, blank=True, null=True)
    nomor_identitas_donatur = models.ForeignKey(Donatur, models.CASCADE, db_column='nomor_identitas_donatur')

    class Meta:
        db_table = 'yayasan'
        managed = False
