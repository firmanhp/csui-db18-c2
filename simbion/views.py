from django.shortcuts import render, redirect, reverse
from django.contrib import messages
from django.contrib import auth
from django.contrib.auth.decorators import login_required

from .forms import RegistrationFormIndividualDonor, RegistrationFormYayasan, RegistrationFormMahasiswa, \
    DonaturBuatPaket, DonaturTambahPaket, BeasiswaRegisterForm
from .models import UserManager, SkemaBeasiswa, SyaratBeasiswa, SkemaBeasiswaAktif, Pendaftaran, Donatur, \
    Mahasiswa
from .helpers import hlp_donatur, hlp_mahasiswa, hlp_beasiswa


def get_all_beasiswa():
    beasiswa_list = []
    query_set = SkemaBeasiswaAktif.objects.raw("SELECT * FROM " + SkemaBeasiswaAktif._meta.db_table
        + " SBA INNER JOIN " + SkemaBeasiswa._meta.db_table + " SB ON SBA.kode_skema_beasiswa = SB.kode")
    for beasiswa in list(query_set):
        beasiswa_list.append(beasiswa)
    return beasiswa_list


def index(request):
    response = {'beasiswa_list' : get_all_beasiswa()}

    return render(request, 'index.html', response)


def login(request):

    if request.user.is_authenticated:
        return redirect(reverse('index'))

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = auth.authenticate(request, username=username, password=password)
        if user is None:
            messages.error(request, 'Nama pengguna atau kata sandi salah.')
        else:
            auth.login(request, user)
            return redirect(reverse('index'))

    return render(request, 'login.html')


def logout(request):
    auth.logout(request)
    messages.info(request, 'Anda berhasil keluar')
    return redirect(reverse('index'))


def register(request):
    if request.user.is_authenticated:
        return redirect(reverse('index'))

    if request.user.is_authenticated:
        return redirect(reverse('index'))

    return render(request, 'register.html')


def register_mahasiswa(request):
    if request.user.is_authenticated:
        return redirect(reverse('index'))

    if request.method == 'POST':
        form = RegistrationFormMahasiswa(request.POST)

        if not form.is_valid():
            return render(request, 'register_mahasiswa.html', {'form': form})

        cleaned_data = form.cleaned_data
        user = UserManager().create_mahasiswa(**cleaned_data)

        auth.login(request, user)

        messages.success(request, 'Pendaftaran berhasil, selamat datang!')
        return redirect(reverse('index'))

    form = RegistrationFormMahasiswa()

    return render(request, 'register_mahasiswa.html', {'form': form})


def register_donatur_individu(request):
    if request.user.is_authenticated:
        return redirect(reverse('index'))

    if request.method == 'POST':
        form = RegistrationFormIndividualDonor(request.POST)

        if not form.is_valid():
            return render(request, 'register_donatur_individual.html', {'form': form})

        cleaned_data = form.cleaned_data
        user = UserManager().create_donatur_individual_donor(**cleaned_data)

        auth.login(request, user)

        messages.success(request, 'Pendaftaran berhasil, selamat datang!')
        return redirect(reverse('index'))

    form = RegistrationFormIndividualDonor()

    return render(request, 'register_donatur_individual.html', {'form': form})


def register_donatur_yayasan(request):
    if request.user.is_authenticated:
        return redirect(reverse('index'))

    if request.method == 'POST':
        form = RegistrationFormYayasan(request.POST)

        if not form.is_valid():
            return render(request, 'register_donatur_yayasan.html', {'form': form})

        cleaned_data = form.cleaned_data
        user = UserManager().create_donatur_yayasan(**cleaned_data,
                                                    email_yayasan=cleaned_data['email'],
                                                    nama_yayasan=cleaned_data['nama'],
                                                    no_telp_cp=cleaned_data['no_telp'])


        auth.login(request, user)

        messages.success(request, 'Pendaftaran berhasil, selamat datang!')
        return redirect(reverse('index'))

    form = RegistrationFormYayasan()

    return render(request, 'register_donatur_yayasan.html', {'form': form})


@login_required
def donatur_beasiswa_baru(request):
    if not request.user.is_donatur:
        return redirect(reverse('index'))

    return render(request, 'donatur_beasiswa_baru.html')

@login_required
def donatur_beasiswa_baru_membuat_paket(request):
    if not request.user.is_donatur:
        return redirect(reverse('index'))

    if request.method == 'POST':
        form = DonaturBuatPaket(request.POST)

        if not form.is_valid():
            return render(request, 'donatur_beasiswa_baru_membuat_paket.html', {'form' : form})

        cleaned_data = form.cleaned_data

        hlp_donatur.insert_buat_paket_beasiswa(request, cleaned_data)

        messages.success(request, 'Anda telah berhasil membuat paket beasiswa baru!')
        return redirect(reverse('index'))
    else:
        form = DonaturBuatPaket()
        return render(request, 'donatur_beasiswa_baru_membuat_paket.html', {'form' : form})

@login_required
def donatur_beasiswa_baru_tambahkan_paket(request):
    if not request.user.is_donatur:
        return redirect(reverse('index'))

    if request.method == 'POST':
        form = DonaturTambahPaket(request.POST)
        form.update_choices(request.user)
        if not form.is_valid():
            return render(request, 'donatur_beasiswa_baru_tambahkan_paket.html', {'form' : form})
        
        cleaned_data = form.cleaned_data

        hlp_donatur.insert_tambahkan_paket_beasiswa(request.user, cleaned_data)

        messages.success(request, 'Anda telah berhasil menambahkan paket beasiswa baru!')
        return redirect(reverse('index'))
    else:
        form = DonaturTambahPaket()
        form.update_choices(request.user)
        return render(request, 'donatur_beasiswa_baru_tambahkan_paket.html', {'form' : form})

@login_required
def donatur_list_beasiswa(request):
    if not request.user.is_donatur:
        return redirect(reverse('index'))

    query_set = Donatur.objects.raw("SELECT * FROM " + Donatur._meta.db_table + " WHERE username = %s",
                                        [str(request.user)])
    nomor_identitas = list(query_set)[0].nomor_identitas

    query_set = SkemaBeasiswaAktif.objects.raw("SELECT * FROM " + SkemaBeasiswaAktif._meta.db_table
        + " SBA INNER JOIN " + SkemaBeasiswa._meta.db_table + " SB ON SBA.kode_skema_beasiswa = SB.kode WHERE"
        + " SB.nomor_identitas_donatur = %s", [nomor_identitas])

    beasiswa_list = list(query_set)

    return render(request, 'donatur_list_beasiswa.html', {'is_donatur' : True, 'beasiswa_list' : beasiswa_list})

@login_required
def donatur_beasiswa(request, id, no_urut):
    if not request.user.is_donatur:
        return redirect(reverse('index'))

    query_set = SkemaBeasiswaAktif.objects.raw("SELECT * FROM " + SkemaBeasiswaAktif._meta.db_table
        + " SBA INNER JOIN " + Pendaftaran._meta.db_table + " P ON SBA.kode_skema_beasiswa = P.kode_skema_beasiswa" 
        + " INNER JOIN " + Mahasiswa._meta.db_table + " M ON P.npm = M.npm"
        + " WHERE SBA.kode_skema_beasiswa = %s AND SBA.no_urut = %s AND SBA.no_urut = P.no_urut", [id, no_urut])

    pendaftar_list = list(query_set)
    pendaftar_list.sort(key=lambda p: p.waktu_daftar)

    query_set = SkemaBeasiswaAktif.objects.raw("SELECT * FROM " + SkemaBeasiswaAktif._meta.db_table
        + " SBA WHERE SBA.kode_skema_beasiswa = %s AND SBA.no_urut = %s", [id, no_urut])
    nama_skema = list(query_set)[0].kode_skema_beasiswa.nama

    pendaftar_list_final = []
    for i in range(1, len(list(pendaftar_list)) + 1):
        pendaftar_list_final.append((pendaftar_list[i - 1], i))

    return render(request, 'donatur_beasiswa.html', {'is_donatur' : True, 'pendaftar_list' : pendaftar_list_final, 
                                                    'nama_skema' : nama_skema, 'no_urut' : no_urut})

@login_required
def donatur_tolak_mahasiswa(request, id, no_urut, npm):
    if not request.user.is_donatur:
        return redirect(reverse('index'))

    hlp_donatur.tolak_mahasiswa(id, no_urut, npm)

    return redirect(reverse('donatur_beasiswa', args=(id, no_urut)))

@login_required
def donatur_terima_mahasiswa(request, id, no_urut, npm):
    if not request.user.is_donatur:
        return redirect(reverse('index'))

    hlp_donatur.terima_mahasiswa(id, no_urut, npm)

    return redirect(reverse('donatur_beasiswa', args=(id, no_urut)))

def beasiswa_detail(request, id):
    query_set = SkemaBeasiswa.objects.raw("SELECT * FROM " + SkemaBeasiswa._meta.db_table + " WHERE kode = %s", [id])
    beasiswa = list(query_set)[0]
    query_set = SyaratBeasiswa.objects.raw("SELECT * FROM " + SyaratBeasiswa._meta.db_table + " WHERE kode_beasiswa = %s", [id])
    syarat_beasiswa = list(query_set)
    return render(request, 'beasiswa_detail.html', {'beasiswa' : beasiswa, 'syarat_beasiswa' : syarat_beasiswa})

@login_required
def mahasiswa_beasiswa_detail(request, id, no_urut):
    if not request.user.is_mahasiswa:
        return redirect(reverse('index'))

    query_set = SkemaBeasiswa.objects.raw("SELECT * FROM " + SkemaBeasiswa._meta.db_table + " WHERE kode = %s", [id])
    if len(list(query_set)) == 0:
        return redirect(reverse('index'))
    beasiswa = list(query_set)[0]

    query_set = SkemaBeasiswaAktif.objects.raw("SELECT * FROM " + SkemaBeasiswaAktif._meta.db_table + " WHERE "
                                               + "kode_skema_beasiswa = %s AND "
                                               + "no_urut = %s",
                                               [id, no_urut])
    if len(list(query_set)) == 0:
        return redirect(reverse('index'))
    skema_beasiswa_aktif = list(query_set)[0]

    query_set = SyaratBeasiswa.objects.raw("SELECT * FROM " + SyaratBeasiswa._meta.db_table + " WHERE kode_beasiswa = %s", [id])
    syarat_beasiswa = list(query_set)
    return render(request, 'beasiswa_detail.html', {
        'beasiswa' : beasiswa,
        'syarat_beasiswa' : syarat_beasiswa,
        'skema_beasiswa_aktif': skema_beasiswa_aktif})

@login_required
def beasiswa_register(request):
    if not request.user.is_mahasiswa:
        return redirect(reverse('index'))

    mahasiswa = hlp_mahasiswa.get_user_mahasiswa(request.user)

    if request.method == 'POST':
        form = BeasiswaRegisterForm(request.POST, initial={
            'kode_beasiswa': request.GET.get('id', None),
            'npm': mahasiswa.npm,
            'email': mahasiswa.email
        })
        if not form.is_valid():
            return render(request, 'beasiswa_register.html', {
                'mahasiswa': mahasiswa,
                'form': form
            })
        cleaned_data = form.cleaned_data
        kode = str(cleaned_data['kode_beasiswa']).split('-')
        kode_skema_beasiswa, no_urut = kode[0], kode[1]
        npm = mahasiswa.npm

        query_set = Pendaftaran.objects.raw("SELECT * FROM " + Pendaftaran._meta.db_table + " WHERE "
                                            + "kode_skema_beasiswa = %s AND "
                                            + "no_urut = %s AND "
                                            + "npm = %s",
                                            [kode_skema_beasiswa, no_urut, npm])
        if len(list(query_set)) > 0:
            messages.error(request, 'Anda sudah pernah mendaftar pada beasiswa ini!')
            return redirect(reverse('index'))

        skema_beasiswa_aktif = hlp_beasiswa.get_skema_beasiswa_aktif(kode_skema_beasiswa, no_urut)
        hlp_beasiswa.register_beasiswa(mahasiswa, skema_beasiswa_aktif)

        messages.success(request, 'Pendaftaran berhasil.')
        return redirect(reverse('index'))


    form = BeasiswaRegisterForm(initial={
        'kode_beasiswa': request.GET.get('id', None),
        'npm': mahasiswa.npm,
        'email': mahasiswa.email})

    return render(request, 'beasiswa_register.html', {
        'mahasiswa' : mahasiswa,
        'form': form
    })

def create_admin(request):
    admin = UserManager().create_admin(username='admin',
                                       password='aaa123aaa123')

    auth.login(request, admin)
    return redirect(reverse('index'))