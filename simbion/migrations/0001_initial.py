# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2018-05-19 19:09
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import simbion.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0008_alter_user_username_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='Pengguna',
            fields=[
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('username', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('role', models.CharField(max_length=20)),
                ('is_superuser', models.BooleanField(default=False)),
            ],
            options={
                'db_table': 'pengguna',
            },
            managers=[
                ('objects', simbion.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Donatur',
            fields=[
                ('nomor_identitas', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('email', models.CharField(max_length=50)),
                ('nama', models.CharField(max_length=50)),
                ('npwp', models.CharField(max_length=20)),
                ('no_telp', models.CharField(blank=True, max_length=20, null=True)),
                ('alamat', models.CharField(max_length=50)),
            ],
            options={
                'db_table': 'donatur',
                'managed': False,
            },
        ),
        migrations.RunSQL(
            """
            CREATE TABLE DONATUR(
                nomor_identitas VARCHAR(20) NOT NULL,
                email VARCHAR(50) NOT NULL,
                nama VARCHAR(50) NOT NULL,
                npwp CHAR(20) NOT NULL,
                no_telp VARCHAR(20),
                alamat VARCHAR(50) NOT NULL,
                username VARCHAR(20) NOT NULL,
                PRIMARY KEY (nomor_identitas),
                FOREIGN KEY (username)
                    REFERENCES PENGGUNA(username) ON DELETE CASCADE ON UPDATE CASCADE
            );
            """
        ),
        migrations.CreateModel(
            name='IndividualDonor',
            fields=[
                ('nik', models.CharField(max_length=16, primary_key=True, serialize=False)),
            ],
            options={
                'db_table': 'individual_donor',
                'managed': False,
            },
        ),
        migrations.RunSQL(
            """
            CREATE TABLE INDIVIDUAL_DONOR(
                nik CHAR(16) NOT NULL,
                nomor_identitas_donatur VARCHAR(20) NOT NULL,
                PRIMARY KEY (nik),
                FOREIGN KEY (nomor_identitas_donatur)
                    REFERENCES DONATUR(nomor_identitas) ON DELETE CASCADE ON UPDATE CASCADE
            );
            """
        ),
        migrations.CreateModel(
            name='Mahasiswa',
            fields=[
                ('npm', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('email', models.CharField(max_length=50)),
                ('nama', models.CharField(max_length=50)),
                ('no_telp', models.CharField(blank=True, max_length=20, null=True)),
                ('alamat_tinggal', models.CharField(max_length=50)),
                ('alamat_domisili', models.CharField(max_length=50)),
                ('nama_bank', models.CharField(max_length=50)),
                ('no_rekening', models.CharField(max_length=20)),
                ('nama_pemilik', models.CharField(max_length=20)),
            ],
            options={
                'db_table': 'mahasiswa',
                'managed': False,
            },
        ),
        migrations.RunSQL(
            """
            CREATE TABLE MAHASISWA(
                npm VARCHAR(20) NOT NULL,
                email VARCHAR(50) NOT NULL,
                nama VARCHAR(50) NOT NULL,
                no_telp VARCHAR(20),
                alamat_tinggal VARCHAR(50) NOT NULL,
                alamat_domisili VARCHAR(50) NOT NULL,
                nama_bank VARCHAR(50) NOT NULL,
                no_rekening VARCHAR(20) NOT NULL,
                nama_pemilik VARCHAR(20) NOT NULL,
                username VARCHAR(20) NOT NULL,
                PRIMARY KEY (npm),
                FOREIGN KEY (username)
                    REFERENCES PENGGUNA(username) ON DELETE CASCADE ON UPDATE CASCADE
            );
            """
        ),
        migrations.CreateModel(
            name='RiwayatAkademik',
            fields=[
                ('no_urut', models.IntegerField(primary_key=True, serialize=False)),
                ('semester', models.CharField(max_length=1)),
                ('tahun_ajaran', models.CharField(max_length=9)),
                ('jumlah_sks', models.IntegerField()),
                ('ips', models.FloatField()),
                ('lampiran', models.CharField(max_length=50)),
            ],
            options={
                'db_table': 'riwayat_akademik',
                'managed': False,
            },
        ),
        migrations.RunSQL(
            """
            CREATE TABLE RIWAYAT_AKADEMIK(
                no_urut INTEGER NOT NULL,
                npm VARCHAR(20) NOT NULL,
                semester CHAR(1) NOT NULL,
                tahun_ajaran CHAR(9) NOT NULL,
                jumlah_sks INTEGER NOT NULL,
                IPS DOUBLE PRECISION NOT NULL,
                lampiran VARCHAR(50) NOT NULL,
                PRIMARY KEY (no_urut, npm),
                FOREIGN KEY (npm)
                    REFERENCES MAHASISWA(npm) ON DELETE CASCADE ON UPDATE CASCADE
            );
            """
        ),
        migrations.CreateModel(
            name='SkemaBeasiswa',
            fields=[
                ('kode', models.IntegerField(primary_key=True, serialize=False)),
                ('nama', models.CharField(max_length=50)),
                ('jenis', models.CharField(max_length=20)),
                ('deskripsi', models.CharField(max_length=50)),
            ],
            options={
                'db_table': 'skema_beasiswa',
                'managed': False,
            },
        ),
        migrations.RunSQL(
            """
            CREATE TABLE SKEMA_BEASISWA(
                kode INTEGER NOT NULL,
                nama VARCHAR(50) NOT NULL,
                jenis VARCHAR(20) NOT NULL,
                deskripsi VARCHAR(50) NOT NULL,
                nomor_identitas_donatur VARCHAR(20) NOT NULL,
                PRIMARY KEY (kode),
                FOREIGN KEY (nomor_identitas_donatur)
                    REFERENCES DONATUR(nomor_identitas) ON DELETE CASCADE ON UPDATE CASCADE
            );
            """
        ),
        migrations.RunSQL(
            """
            CREATE TABLE SKEMA_BEASISWA_AKTIF(
                kode_skema_beasiswa INTEGER NOT NULL,
                no_urut INTEGER NOT NULL,
                tgl_mulai_pendaftaran DATE NOT NULL,
                tgl_tutup_pendaftaran DATE NOT NULL,
                periode_penerimaan VARCHAR(50) NOT NULL,
                status VARCHAR(20) NOT NULL,
                jumlah_pendaftar INTEGER NOT NULL,
                total_pembayaran BIGINT NOT NULL,
                PRIMARY KEY (kode_skema_beasiswa, no_urut),
                FOREIGN KEY (kode_skema_beasiswa)
                    REFERENCES SKEMA_BEASISWA(kode) ON DELETE CASCADE ON UPDATE CASCADE
            );
            """
        ),
        migrations.CreateModel(
            name='Pembayaran',
            fields=[
                ('urutan', models.IntegerField(primary_key=True, serialize=False)),
                ('no_urut_skema_beasiswa_aktif', models.IntegerField()),
                ('keterangan', models.CharField(max_length=50)),
                ('tgl_bayar', models.DateField()),
                ('nominal', models.IntegerField()),
            ],
            options={
                'db_table': 'pembayaran',
                'managed': False,
            },
        ),
        migrations.RunSQL(
            """
            CREATE TABLE PEMBAYARAN(
                urutan INTEGER NOT NULL,
                kode_skema_beasiswa INTEGER NOT NULL,
                no_urut_skema_beasiswa_aktif INTEGER NOT NULL,
                npm VARCHAR(20) NOT NULL,
                keterangan VARCHAR(50) NOT NULL,
                tgl_bayar DATE NOT NULL,
                nominal INTEGER NOT NULL,
                PRIMARY KEY (urutan, kode_skema_beasiswa, no_urut_skema_beasiswa_aktif, npm),
                FOREIGN KEY (kode_skema_beasiswa, no_urut_skema_beasiswa_aktif)
                    REFERENCES SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa, no_urut) ON DELETE CASCADE ON UPDATE CASCADE,
                FOREIGN KEY (npm)
                    REFERENCES MAHASISWA(npm) ON DELETE CASCADE ON UPDATE CASCADE    
            );
            """
        ),
        migrations.CreateModel(
            name='Pengumuman',
            fields=[
                ('tanggal', models.DateField(primary_key=True, serialize=False)),
                ('kode_skema_beasiswa', models.IntegerField()),
                ('judul', models.CharField(max_length=20)),
                ('isi', models.CharField(max_length=255)),
            ],
            options={
                'db_table': 'pengumuman',
                'managed': False,
            },
        ),
        migrations.RunSQL(
            """
            CREATE TABLE PENGUMUMAN(
                tanggal DATE NOT NULL,
                no_urut_skema_beasiswa_aktif INTEGER NOT NULL,
                kode_skema_beasiswa INTEGER NOT NULL,
                username VARCHAR(20) NOT NULL,
                judul VARCHAR(20) NOT NULL,
                isi VARCHAR(255) NOT NULL,
                PRIMARY KEY (tanggal, no_urut_skema_beasiswa_aktif, kode_skema_beasiswa, username),
                FOREIGN KEY (no_urut_skema_beasiswa_aktif, kode_skema_beasiswa)
                    REFERENCES SKEMA_BEASISWA_AKTIF(no_urut, kode_skema_beasiswa) ON DELETE CASCADE ON UPDATE CASCADE,
                FOREIGN KEY (username)
                    REFERENCES PENGGUNA(username) ON DELETE CASCADE ON UPDATE CASCADE
            );
            """
        ),
        migrations.CreateModel(
            name='SyaratBeasiswa',
            fields=[
                ('kode_beasiswa',
                 models.ForeignKey(db_column='kode_beasiswa', on_delete=django.db.models.deletion.CASCADE,
                                   primary_key=True, serialize=False, to='simbion.SkemaBeasiswa')),
                ('syarat', models.CharField(max_length=50)),
            ],
            options={
                'db_table': 'syarat_beasiswa',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='TempatWawancara',
            fields=[
                ('kode', models.IntegerField(primary_key=True, serialize=False)),
                ('nama', models.CharField(max_length=50)),
                ('lokasi', models.CharField(max_length=50)),
            ],
            options={
                'db_table': 'tempat_wawancara',
                'managed': False,
            },
        ),
        migrations.RunSQL(
            """
            CREATE TABLE TEMPAT_WAWANCARA(
                kode INTEGER NOT NULL,
                nama VARCHAR(50) NOT NULL,
                lokasi VARCHAR(50) NOT NULL,
                PRIMARY KEY (kode)
            );
            """
        ),
        migrations.CreateModel(
            name='Wawancara',
            fields=[
                ('no_urut_skema_beasiswa_aktif', models.IntegerField(primary_key=True, serialize=False)),
                ('jadwal', models.DateTimeField()),
            ],
            options={
                'db_table': 'wawancara',
                'managed': False,
            },
        ),
        migrations.RunSQL(
            """
            CREATE TABLE WAWANCARA(
                no_urut_skema_beasiswa_aktif INTEGER NOT NULL,
                kode_skema_beasiswa INTEGER NOT NULL,
                jadwal TIMESTAMP NOT NULL,
                kode_tempat_wawancara INTEGER NOT NULL,
                PRIMARY KEY (no_urut_skema_beasiswa_aktif, kode_skema_beasiswa, jadwal),
                FOREIGN KEY (kode_tempat_wawancara)
                    REFERENCES TEMPAT_WAWANCARA(kode) ON DELETE CASCADE ON UPDATE CASCADE,
                FOREIGN KEY (kode_skema_beasiswa, no_urut_skema_beasiswa_aktif)
                    REFERENCES SKEMA_BEASISWA_AKTIF(kode_skema_beasiswa, no_urut) ON DELETE CASCADE ON UPDATE CASCADE
            );
            """
        ),
        migrations.CreateModel(
            name='Yayasan',
            fields=[
                ('no_sk_yayasan', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('email', models.CharField(max_length=50)),
                ('nama', models.CharField(max_length=50)),
                ('no_telp_cp', models.CharField(blank=True, max_length=20, null=True)),
            ],
            options={
                'db_table': 'yayasan',
                'managed': False,
            },
        ),
        migrations.RunSQL(
            """
            CREATE TABLE YAYASAN(
                no_sk_yayasan VARCHAR(20) NOT NULL,
                email VARCHAR(50) NOT NULL,
                nama VARCHAR(50) NOT NULL,
                no_telp_cp VARCHAR(20),
                nomor_identitas_donatur VARCHAR(20) NOT NULL,
                PRIMARY KEY (no_sk_yayasan),
                FOREIGN KEY (nomor_identitas_donatur)
                    REFERENCES DONATUR(nomor_identitas) ON DELETE CASCADE ON UPDATE CASCADE
            );
            """
        ),
        migrations.CreateModel(
            name='Admin',
            fields=[
                ('username', models.ForeignKey(db_column='username', on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'admin',
                'managed': False,
            },
        ),
        migrations.RunSQL(
            """
            CREATE TABLE ADMIN(
                username VARCHAR(20) NOT NULL,
                PRIMARY KEY (username),
                FOREIGN KEY (username)
                    REFERENCES PENGGUNA(username) ON DELETE CASCADE ON UPDATE CASCADE
            );
            """
        ),
        migrations.CreateModel(
            name='SkemaBeasiswaAktif',
            fields=[
                ('kode_skema_beasiswa', models.ForeignKey(db_column='kode_skema_beasiswa', on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='simbion.SkemaBeasiswa')),
                ('no_urut', models.IntegerField()),
                ('tgl_mulai_pendaftaran', models.DateField()),
                ('tgl_tutup_pendaftaran', models.DateField()),
                ('periode_penerimaan', models.CharField(max_length=50)),
                ('status', models.CharField(max_length=20)),
                ('jumlah_pendaftar', models.IntegerField()),
                ('total_pembayaran', models.BigIntegerField()),
            ],
            options={
                'db_table': 'skema_beasiswa_aktif',
                'managed': False,
            },
        ),
        migrations.RunSQL(
            """
            CREATE TABLE SYARAT_BEASISWA(
                kode_beasiswa INTEGER NOT NULL,
                syarat VARCHAR(50) NOT NULL,
                PRIMARY KEY (kode_beasiswa, syarat),
                FOREIGN KEY (kode_beasiswa)
                    REFERENCES SKEMA_BEASISWA(kode) ON DELETE CASCADE ON UPDATE CASCADE
            );
            """
        ),
        migrations.AddField(
            model_name='pengguna',
            name='groups',
            field=models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups'),
        ),
        migrations.AddField(
            model_name='pengguna',
            name='user_permissions',
            field=models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions'),
        ),
        migrations.CreateModel(
            name='Pendaftaran',
            fields=[
                ('no_urut', models.ForeignKey(db_column='no_urut', on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='simbion.SkemaBeasiswaAktif')),
                ('kode_skema_beasiswa', models.IntegerField()),
                ('waktu_daftar', models.DateTimeField()),
                ('status_daftar', models.CharField(max_length=20)),
                ('status_terima', models.CharField(max_length=20)),
            ],
            options={
                'db_table': 'pendaftaran',
                'managed': False,
            },
        ),
        migrations.RunSQL(
            """
            CREATE TABLE PENDAFTARAN(
                no_urut INTEGER NOT NULL,
                kode_skema_beasiswa INTEGER NOT NULL,
                npm VARCHAR(20) NOT NULL,
                waktu_daftar TIMESTAMP NOT NULL,
                status_daftar VARCHAR(20) NOT NULL,
                status_terima VARCHAR(20) NOT NULL,
                PRIMARY KEY (no_urut, kode_skema_beasiswa, npm),
                FOREIGN KEY (no_urut, kode_skema_beasiswa)
                    REFERENCES SKEMA_BEASISWA_AKTIF(no_urut, kode_skema_beasiswa) ON DELETE CASCADE ON UPDATE CASCADE,
                FOREIGN KEY (npm)
                    REFERENCES MAHASISWA(npm) ON DELETE CASCADE ON UPDATE CASCADE
            );
            """
        ),
    ]
