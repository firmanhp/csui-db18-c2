from django import forms
from ..helpers import hlp_pengguna

REQUIRED_MSG = 'Wajib diisi'
CSS_CLASSES = 'form-control'


class RegistrationFormPengguna(forms.Form):

    username = forms.CharField(label='Nama pengguna',
                               max_length=20,
                               error_messages={
                                   'required': REQUIRED_MSG,
                                   'max_length': 'Maksimal 20 karakter'
                               },
                               widget=forms.TextInput(attrs={'class': CSS_CLASSES,
                                                             'placeholder': 'Masukkan nama pengguna'}))

    password = forms.CharField(label='Kata sandi',
                               max_length=20,
                               min_length=8,
                               error_messages={
                                   'required': REQUIRED_MSG,
                                   'max_length': 'Maksimal 20 karakter',
                                   'min_length': 'Minimal 8 karakter'
                               },
                               widget=forms.PasswordInput(attrs={'class': CSS_CLASSES,
                                                                 'placeholder': 'Masukkan kata sandi'}))

    def clean_username(self):
        username = self.cleaned_data.get('username')
        if not all(x.isdigit() or x.isalpha() for x in username):
            raise forms.ValidationError('Mohon gunakan hanya karakter alfabet atau angka')

        if hlp_pengguna.is_username_used(username):
            raise forms.ValidationError('Nama pengguna sudah digunakan')

        return username

    def clean_password(self):
        password = self.cleaned_data.get('password')

        if not all(x.isdigit() or x.isalpha() for x in password):
            raise forms.ValidationError('Mohon gunakan hanya karakter alfabet atau angka')

        if not (any(x.isdigit() for x in password) and any(x.isalpha() for x in password)):
            raise forms.ValidationError('Mohon gunakan karakter alfabet dan angka sekaligus')

        return password
