from django import forms

from ..helpers import hlp_beasiswa

REQUIRED_MSG = 'Wajib diisi'
CSS_CLASSES = 'form-control'

class BeasiswaRegisterForm(forms.Form):
    kode_beasiswa = forms.ChoiceField(
        label='Beasiswa',
        error_messages={
            'required': REQUIRED_MSG,
            'invalid_choice': 'Beasiswa tidak valid'
        },
        widget=forms.Select(attrs={'class': CSS_CLASSES})
    )

    npm = forms.CharField(
        label='NPM',
        error_messages={
            'required': REQUIRED_MSG,
        },
        disabled=True,
        widget=forms.TextInput(attrs={'class': CSS_CLASSES})
    )

    email = forms.EmailField(
        label='Email',
        error_messages={
            'required': REQUIRED_MSG,
            'invalid': 'Email tidak valid'
        },
        disabled=True,
        widget=forms.EmailInput(attrs={'class': CSS_CLASSES})
    )

    ips = forms.FloatField(
        label='Indeks Prestasi Semester',
        min_value=0,
        max_value=4,
        error_messages={
            'required': REQUIRED_MSG,
            'max_value': 'Indeks tidak boleh lebih dari 4',
            'min_value': 'Indeks tidak boleh lebih kecil dari 0',
            'invalid': 'Indeks tidak valid'
        },
        widget=forms.NumberInput(attrs={'class': CSS_CLASSES,
                                        'placeholder': 'Masukkan indeks Anda'})
    )

    def __init__(self, *args, **kwargs):
        super(BeasiswaRegisterForm, self).__init__(*args, **kwargs)
        self.fields['kode_beasiswa'].choices = [(str(skema.kode_skema_beasiswa.kode) + '-' + str(skema.no_urut),
                                                 str(skema.kode_skema_beasiswa.kode) + ' - ' + str(skema.no_urut) + ' ' + str(skema.kode_skema_beasiswa.nama))
                                                for skema in hlp_beasiswa.get_open_skema_aktifs()]
