from django import forms
from .form_register_donatur import RegistrationFormDonatur
from ..helpers import hlp_individual_donor

REQUIRED_MSG = 'Wajib diisi'
CSS_CLASSES = 'form-control'


class RegistrationFormIndividualDonor(RegistrationFormDonatur):

    nik = forms.CharField(label='NIK',
                          max_length=16,
                          min_length=16,
                          error_messages={
                              'required': REQUIRED_MSG,
                              'max_length': 'Harus 16 karakter',
                              'min_length': 'Harus 16 karakter',
                          },
                          widget=forms.TextInput(attrs={'class': CSS_CLASSES,
                                                        'placeholder': 'Masukkan NIK'}))

    def clean_nik(self):
        nik = self.cleaned_data.get('nik')

        if not all(x.isdigit() for x in nik):
            raise forms.ValidationError('Mohon gunakan hanya karakter angka')

        if hlp_individual_donor.is_nik_used(nik):
            raise forms.ValidationError('NIK sudah digunakan')

        return nik