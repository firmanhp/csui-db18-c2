from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from simbion.models import SkemaBeasiswa

class DonaturBuatPaket(forms.Form):
    kode = forms.IntegerField(
        widget=forms.NumberInput(attrs={'class':'form-control', 'name':'kode', 'id':'kode', 'min' : 0, 'max' : 1000000000}),
        label='Kode',
        error_messages={
            'required': 'Wajib diisi',
        }
    )
    nama_paket = forms.CharField(
        max_length=50,
        widget=forms.TextInput(attrs={'class':'form-control', 'name':'nama_paket', 'id':'nama_paket'}),
        label='Nama Paket Beasiswa',
        error_messages={
            'required': 'Wajib diisi',
            'max_length': 'Maksimal 50 karakter'
        }   
    )
    jenis_beasiswa = forms.ChoiceField(
        widget=forms.Select(attrs={'class':'form-control', 'name':'nama_paket', 'id':'nama_paket'}), 
        choices=[('Pilih Jenis Beasiswa','Pilih Jenis Beasiswa',), ('Penuh', 'Penuh',), ('Parsial', 'Parsial')],
        label='Jenis Beasiswa',
        error_messages={
            'required': 'Wajib diisi',
        }
    )
    deskripsi = forms.CharField(
        max_length=50,
        widget=forms.TextInput(attrs={'class':'form-control', 'name':'deskripsi', 'id':'deskripsi'}),
        label='Deskripsi',
        error_messages={
            'required': 'Wajib diisi',
            'max_length': 'Maksimal 50 karakter',
        }
    )

    def clean_kode(self):
        kode = self.cleaned_data.get('kode')

        is_kode_already_exists = "SELECT * FROM %s WHERE kode = %s" % (SkemaBeasiswa._meta.db_table, kode)

        if len(list(SkemaBeasiswa.objects.raw(is_kode_already_exists))) > 0:
            raise forms.ValidationError(_('Terdapat Skema Beasiswa yang menggunakan kode tersebut.'))

        return kode

    def clean_jenis_beasiswa(self):
        jenis_beasiswa = self.cleaned_data.get('jenis_beasiswa')

        if jenis_beasiswa == 'Pilih Jenis Beasiswa':
            raise forms.ValidationError(_('Jenis beasiswa harus dipilih.'))

        return jenis_beasiswa
