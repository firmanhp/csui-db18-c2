from django import forms
from django.utils.translation import ugettext_lazy as _
from simbion.helpers import hlp_donatur
from simbion.models import SkemaBeasiswaAktif

class DonaturTambahPaket(forms.Form):
    kode_beasiswa = forms.ChoiceField(
        widget=forms.Select(attrs={'class':'form-control col-sm-12', 'name':'kode_beasiswa', 'id':'kode_beasiswa'}),
        label='Kode Beasiswa',
        error_messages={
            'required': 'Wajib diisi',
        }
    )
    nomor_urut = forms.IntegerField(
        widget=forms.NumberInput(attrs={'class':'form-control', 'name':'nomor_urut', 'id':'nomor_urut', 'min' : 0, 'max' : 1000000000}),
        label='Nomor Urut',
        error_messages={
            'required': 'Wajib diisi',
        }
    )
    tanggal_mulai = forms.DateField(
        widget=forms.DateInput(attrs={'type' : 'date', 'class':'form-control', 'name':'tanggal_mulai', 'id':'tanggal_mulai'}),
        label='Tanggal Mulai Pendaftaran',
        error_messages={
            'required': 'Wajib diisi',
        }
    )
    tanggal_tutup = forms.DateField(
        widget=forms.DateInput(attrs={'type':'date', 'class':'form-control', 'name':'tanggal_tutup', 'id':'tanggal_tutup'}),
        label='Tanggal Tutup Pendaftaran',
        error_messages={
            'required': 'Wajib diisi',
        }
    )    

    def __init__(self, *args, **kwargs):
        super(DonaturTambahPaket, self).__init__(*args, **kwargs)
        self.fields['kode_beasiswa'].choices = [('Pilih Kode Beasiswa', 'Pilih Kode Beasiswa')]

    def update_choices(self, username):
        self.fields['kode_beasiswa'].choices = [(kode, kode) for kode in hlp_donatur.get_kode_beasiswa(username)]

    def clean_kode_beasiswa(self):
        kode_beasiswa = self.cleaned_data.get('kode_beasiswa')

        if kode_beasiswa == 'Pilih Kode Beasiswa' or kode_beasiswa == None:
            raise forms.ValidationError(_('Kode beasiswa harus dipilih.'))

        return kode_beasiswa

    def clean_nomor_urut(self):
        kode_beasiswa = self.cleaned_data.get('kode_beasiswa')
        nomor_urut = self.cleaned_data.get('nomor_urut')

        if not (kode_beasiswa == 'Pilih Kode Beasiswa' or kode_beasiswa == None):
            is_pair_already_exists = "SELECT * FROM %s WHERE kode_skema_beasiswa = %s AND no_urut = %s" % (SkemaBeasiswaAktif._meta.db_table, kode_beasiswa, nomor_urut)
            if len(list(SkemaBeasiswaAktif.objects.raw(is_pair_already_exists))) > 0:
                raise forms.ValidationError(_('Pasangan Kode Beasiswa dan Nomor Urut sudah terdaftar.'))

        return nomor_urut

    def clean_tanggal_tutup(self):
        tanggal_mulai = self.cleaned_data.get('tanggal_mulai')
        tanggal_tutup = self.cleaned_data.get('tanggal_tutup')

        if tanggal_mulai >= tanggal_tutup:
            raise forms.ValidationError(_('Tanggal tutup harus berada setelah dari tanggal mulai'))

        return tanggal_tutup