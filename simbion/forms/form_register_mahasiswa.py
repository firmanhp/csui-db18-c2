from django import forms
from .form_register_pengguna import RegistrationFormPengguna
from ..helpers import hlp_mahasiswa

REQUIRED_MSG = 'Wajib diisi'
CSS_CLASSES = 'form-control'


class RegistrationFormMahasiswa(RegistrationFormPengguna):

    npm = forms.CharField(label='NPM',
                          max_length=20,
                          error_messages={
                              'required': REQUIRED_MSG,
                              'max_length': 'Maksimal 20 digit'
                          },
                          widget=forms.NumberInput(attrs={'class': CSS_CLASSES,
                                                          'placeholder': 'Masukkan NPM'}))

    email = forms.EmailField(label='Email',
                             max_length=50,
                             error_messages={
                                 'required': REQUIRED_MSG,
                                 'invalid': 'Email tidak valid',
                                 'max_length': 'Maksimal 50 karakter'
                             },
                             widget=forms.EmailInput(attrs={'class': CSS_CLASSES,
                                                            'placeholder': 'Masukkan email'}))

    nama = forms.CharField(label='Nama lengkap',
                           max_length=50,
                           error_messages={
                               'required': REQUIRED_MSG,
                               'max_length': 'Maksimal 50 karakter'
                           },
                           widget=forms.TextInput(attrs={'class': CSS_CLASSES,
                                                         'placeholder': 'Masukkan nama lengkap'}))

    no_telp = forms.CharField(label='Nomor telepon',
                              max_length=20,
                              required=False,
                              error_messages={
                                  'max_length': 'Maksimal 20 digit'
                              },
                              widget=forms.TextInput(attrs={'class': CSS_CLASSES,
                                                            'placeholder': 'Masukkan nomor telepon'}))

    alamat_tinggal = forms.CharField(label='Alamat tempat tinggal',
                                     max_length=50,
                                     error_messages={
                                         'required': REQUIRED_MSG,
                                         'max_length': 'Maksimal 50 karakter'
                                     },
                                     widget=forms.Textarea(attrs={'class': CSS_CLASSES,
                                                                  'placeholder': 'Masukkan alamat tempat tinggal'}))

    alamat_domisili = forms.CharField(label='Alamat domisili',
                                      max_length=50,
                                      error_messages={
                                          'required': REQUIRED_MSG,
                                          'max_length': 'Maksimal 50 karakter'
                                      },
                                      widget=forms.Textarea(attrs={'class': CSS_CLASSES,
                                                                   'placeholder': 'Masukkan alamat domisili'}))

    nama_bank = forms.CharField(label='Instansi Bank',
                                max_length=50,
                                error_messages={
                                    'required': REQUIRED_MSG,
                                    'max_length': 'Maksimal 50 karakter'
                                },
                                widget=forms.TextInput(attrs={'class': CSS_CLASSES,
                                                              'placeholder': 'Masukkan nama instansi bank'}))

    no_rekening = forms.CharField(label='Nomor rekening',
                                  max_length=20,
                                  error_messages={
                                      'required': REQUIRED_MSG,
                                      'max_length': 'Maksimal 20 digit'
                                  },
                                  widget=forms.TextInput(attrs={'class': CSS_CLASSES,
                                                                'placeholder': 'Masukkan nomor rekening'}))

    nama_pemilik = forms.CharField(label='Nama pemilik',
                                   max_length=20,
                                   error_messages={
                                       'required': REQUIRED_MSG,
                                       'max_length': 'Mksimal 20 karakter'
                                   },
                                   widget=forms.TextInput(attrs={'class': CSS_CLASSES,
                                                                 'placeholder': 'Masukkan nama pemilik'}))

    def clean_npm(self):
        npm = self.cleaned_data.get('npm')

        if not all(x.isdigit() for x in npm):
            raise forms.ValidationError('Mohon gunakan hanya karakter angka')

        if hlp_mahasiswa.is_npm_used(npm):
            raise forms.ValidationError('NPM sudah digunakan')

        return npm

    def clean_nama(self):
        nama = self.cleaned_data.get('nama')

        if not all(x.isalpha() or x.isspace() for x in nama):
            raise forms.ValidationError('Mohon gunakan hanya karakter alfabet atau spasi')

        return nama

    def clean_no_telp(self):
        no_telp = self.cleaned_data.get('no_telp')

        if not all(x.isdigit() for x in no_telp):
            raise forms.ValidationError('Mohon gunakan hanya karakter angka')

        return no_telp

    def clean_nama_bank(self):
        nama_bank = self.cleaned_data.get('nama_bank')

        if not all(x.isalpha() or x.isspace() for x in nama_bank):
            raise forms.ValidationError('Mohon gunakan hanya karakter alfabet atau spasi')

        return nama_bank

    def clean_no_rekening(self):
        no_rekening = self.cleaned_data.get('no_rekening')

        if not all(x.isdigit() for x in no_rekening):
            raise forms.ValidationError('Mohon gunakan hanya karakter angka')

        return no_rekening

    def clean_nama_pemilik(self):
        nama_pemilik = self.cleaned_data.get('nama_pemilik')

        if not all(x.isalpha() or x.isspace() for x in nama_pemilik):
            raise forms.ValidationError('Mohon gunakan hanya karakter alfabet atau spasi')

        return nama_pemilik