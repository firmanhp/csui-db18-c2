from django import forms
from .form_register_pengguna import RegistrationFormPengguna
from ..helpers import hlp_donatur

REQUIRED_MSG = 'Wajib diisi'
CSS_CLASSES = 'form-control'

class RegistrationFormDonatur(RegistrationFormPengguna):

    nomor_identitas = forms.CharField(label='Nomor identitas',
                                      max_length=20,
                                      error_messages={
                                          'required': REQUIRED_MSG,
                                          'max_length': 'Maksimal 20 digit'
                                      },
                                      widget=forms.NumberInput(attrs={'class': CSS_CLASSES,
                                                          'placeholder': 'Masukkan nomor identitas'}))

    email = forms.EmailField(label='Email',
                             max_length=50,
                             error_messages={
                                 'required': REQUIRED_MSG,
                                 'invalid': 'Email tidak valid',
                                 'max_length': 'Maksimal 50 karakter'
                             },
                             widget=forms.EmailInput(attrs={'class': CSS_CLASSES,
                                                            'placeholder': 'Masukkan email'}))

    nama = forms.CharField(label='Nama lengkap',
                           max_length=50,
                           error_messages={
                               'required': REQUIRED_MSG,
                               'max_length': 'Maksimal 50 karakter'
                           },
                           widget=forms.TextInput(attrs={'class': CSS_CLASSES,
                                                         'placeholder': 'Masukkan nama lengkap'}))

    npwp = forms.CharField(label='NPWP',
                           max_length=20,
                           min_length=20,
                           error_messages={
                               'required': REQUIRED_MSG,
                               'max_length': 'Harus 20 digit',
                               'min_length': 'Harus 20 digit'
                           },
                           widget=forms.TextInput(attrs={'class': CSS_CLASSES,
                                                         'placeholder': 'Masukkan NPWP'}))

    no_telp = forms.CharField(label='Nomor telepon',
                              max_length=20,
                              required=False,
                              error_messages={
                                  'max_length': 'Maksimal 20 digit'
                              },
                              widget=forms.TextInput(attrs={'class': CSS_CLASSES,
                                                            'placeholder': 'Masukkan nomor telepon'}))

    alamat = forms.CharField(label='Alamat lengkap',
                             max_length=50,
                             error_messages={
                                 'required': REQUIRED_MSG,
                                 'max_length': 'Maksimal 50 karakter'
                             },
                             widget=forms.Textarea(attrs={'class': CSS_CLASSES,
                                                          'placeholder': 'Masukkan alamat lengkap'}))

    def clean_nomor_identitas(self):
        nomor_identitas = self.cleaned_data.get('nomor_identitas')

        if not all(x.isdigit() for x in nomor_identitas):
            raise forms.ValidationError('Mohon gunakan hanya karakter angka')

        if hlp_donatur.is_nomor_identitas_used(nomor_identitas):
            raise forms.ValidationError('Nomor identitas sudah digunakan')

        return nomor_identitas

    def clean_nama(self):
        nama = self.cleaned_data.get('nama')

        if not all(x.isalpha() or x.isspace() for x in nama):
            raise forms.ValidationError('Mohon gunakan hanya karakter alfabet atau spasi')

        return nama

    def clean_npwp(self):
        no_rekening = self.cleaned_data.get('npwp')

        if not all(x.isdigit() for x in no_rekening):
            raise forms.ValidationError('Mohon gunakan hanya karakter angka')

        return no_rekening

    def clean_no_telp(self):
        no_telp = self.cleaned_data.get('no_telp')

        if not all(x.isdigit() for x in no_telp):
            raise forms.ValidationError('Mohon gunakan hanya karakter angka')

        return no_telp
