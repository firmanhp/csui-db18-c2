from django import forms
from .form_register_donatur import RegistrationFormDonatur
from ..helpers import hlp_yayasan

REQUIRED_MSG = 'Wajib diisi'
CSS_CLASSES = 'form-control'


class RegistrationFormYayasan(RegistrationFormDonatur):

    no_sk_yayasan = forms.CharField(label='Nomor SK yayasan',
                                    max_length=20,
                                    error_messages={
                                        'required': REQUIRED_MSG,
                                        'max_length': 'Maksimal 20 karakter',
                                    },
                                    widget=forms.TextInput(attrs={'class': CSS_CLASSES,
                                                                  'placeholder': 'Masukkan nomor SK yayasan'}))

    no_telp = forms.CharField(label='Nomor telepon contact person',
                              max_length=20,
                              required=False,
                              error_messages={
                                  'max_length': 'Maksimal 20 digit'
                              },
                              widget=forms.TextInput(attrs={'class': CSS_CLASSES,
                                                            'placeholder': 'Masukkan nomor telepon contact person'}))

    alamat = forms.CharField(label='Alamat yayasan',
                             max_length=50,
                             error_messages={
                                 'required': REQUIRED_MSG,
                                 'max_length': 'Maksimal 50 karakter'
                             },
                             widget=forms.Textarea(attrs={'class': CSS_CLASSES,
                                                          'placeholder': 'Masukkan alamat yayasan'}))

    nama = forms.CharField(label='Nama yayasan',
                           max_length=50,
                           error_messages={
                               'required': REQUIRED_MSG,
                               'max_length': 'Maksimal 50 karakter'
                           },
                           widget=forms.TextInput(attrs={'class': CSS_CLASSES,
                                                         'placeholder': 'Masukkan nama yayasan'}))

    def clean_no_sk_yayasan(self):
        no_sk_yayasan = self.cleaned_data.get('no_sk_yayasan')

        if hlp_yayasan.is_no_sk_yayasan_used(no_sk_yayasan):
            raise forms.ValidationError('Nomor SK yayasan sudah digunakan')

        return no_sk_yayasan