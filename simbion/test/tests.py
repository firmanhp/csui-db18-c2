from django.test import TestCase
from django.shortcuts import reverse

from ..helpers import sql
from ..models import UserManager
# Create your tests here.


class SimbionTest(TestCase):

    def setUp(self):

        UserManager().create_mahasiswa(username='mhs',
                                       password='aaa123aaa123',
                                       npm='1606862721',
                                       email='mail@mail.com',
                                       nama='Firman Hadi P',
                                       no_telp='021123123',
                                       alamat_tinggal='Jl. Antah berantah',
                                       alamat_domisili='Jl. Berantah antah',
                                       nama_bank='bni',
                                       no_rekening='123123123',
                                       nama_pemilik='Tukiyem')

        UserManager().create_donatur_individual_donor(username='indiv',
                                                      password='aaa123aaa123',
                                                      nomor_identitas='123412341234',
                                                      email='mail@mail.com',
                                                      nama='Firman Hadi P',
                                                      npwp='1234123412341234',
                                                      no_telp='12341234',
                                                      alamat='Jl. Antah berantah',
                                                      nik='1234123412341234')

    def test_helper_sql_create_insert_command(self):
        insert_command = sql.create_insert_command('TABLE_NAME',
                                                   value1=123,
                                                   value2='haha',
                                                   value3=None)
        self.assertEqual(insert_command, "INSERT INTO TABLE_NAME (value1, value2, value3) VALUES ('123', 'haha', NULL)")

    def test_index_unauthenticated(self):
        response = self.client.get(reverse('index'))

        self.assertTemplateUsed(response, 'index.html')

    def test_index_authenticated_mhs(self):
        self.client.login(username='mhs', password='aaa123aaa123')
        response = self.client.get(reverse('index'))

        self.assertTemplateUsed(response, 'index.html')

    def test_index_authenticated_donatur(self):
        self.client.login(username='indiv', password='aaa123aaa123')
        response = self.client.get(reverse('index'))

        self.assertTemplateUsed(response, 'index.html')

    def test_login_get(self):
        response = self.client.get(reverse('login'))

        self.assertTemplateUsed(response, 'login.html')

    def test_login_post_ok(self):
        response = self.client.post(reverse('login'), {
            'username': 'mhs',
            'password': 'aaa123aaa123',
        })

        self.assertRedirects(response, reverse('index'))

    def test_login_post_notok(self):
        response = self.client.post(reverse('login'), {
            'username': 'mhs',
            'password': 'xxx',
        })

        self.assertTemplateUsed(response, 'login.html')

    def test_logout(self):
        self.client.login(username='mhs', password='aaa123aaa123')
        response = self.client.get(reverse('logout'))

        self.assertRedirects(response, reverse('index'))

    def test_register_not_authenticated(self):
        response = self.client.get(reverse('register'))

        self.assertTemplateUsed(response, 'register.html')

    def test_register_authenticated(self):
        self.client.login(username='mhs', password='aaa123aaa123')
        response = self.client.get(reverse('register'))

        self.assertRedirects(response, reverse('index'))

    def test_register_mahasiswa_get_not_authenticated(self):
        response = self.client.get(reverse('register-mhs'))

        self.assertTemplateUsed(response, 'register_mahasiswa.html')

    def test_register_mahasiswa_get_authenticated(self):
        self.client.login(username='mhs', password='aaa123aaa123')
        response = self.client.get(reverse('register-mhs'))

        self.assertRedirects(response, reverse('index'))

    def test_register_mahasiswa_post_not_valid(self):
        response = self.client.post(reverse('register-mhs'), {
            'username': 'mahasiswa',
            'password': 'aaa123', # minimum 8
        })

        self.assertTemplateUsed(response, 'register_mahasiswa.html')

    def test_register_mahasiswa_post_valid(self):
        response = self.client.post(reverse('register-mhs'), {
            'username': 'mahasiswa',
            'password': 'aaa123aaa123',
            'npm': '123123123',
            'email': 'mail@mail.com',
            'nama': 'Mahasiswa Ku',
            'no_telp': '',
            'alamat_tinggal': 'Jalan antah berantah',
            'alamat_domisili': 'Ya gitu deh',
            'nama_bank': 'Bank bank',
            'no_rekening': '321321312',
            'nama_pemilik': 'Mahasiswa Ku'
        })

        self.assertRedirects(response, reverse('index'))

    def test_register_donatur_individu_get_not_authenticated(self):
        response = self.client.get(reverse('register-donatur-individu'))

        self.assertTemplateUsed(response, 'register_donatur_individual.html')

    def test_register_donatur_individu_get_authenticated(self):
        self.client.login(username='indiv', password='aaa123aaa123')
        response = self.client.get(reverse('register-donatur-individu'))

        self.assertRedirects(response, reverse('index'))

    def test_register_donatur_individu_post_not_valid(self):
        response = self.client.post(reverse('register-donatur-individu'), {
            'username': 'indiv',
            'password': 'aaa123', # minimum 8
        })

        self.assertTemplateUsed(response, 'register_donatur_individual.html')

    def test_register_donatur_individu_post_valid(self):
        response = self.client.post(reverse('register-donatur-individu'), {
            'username': 'individu',
            'password': 'aaa123aaa123',
            'nomor_identitas': '322322',
            'email': 'mail@mail.com',
            'nama': 'Donatur Individu Ku',
            'npwp': '12345123451234512345',
            'no_telp': '',
            'alamat': 'Jalan antah berantah',
            'nik': '1234567812345678'
        })

        self.assertRedirects(response, reverse('index'))

    def test_register_donatur_yayasan_get_not_authenticated(self):
        response = self.client.get(reverse('register-donatur-yayasan'))

        self.assertTemplateUsed(response, 'register_donatur_yayasan.html')

    def test_register_donatur_yayasan_get_authenticated(self):
        self.client.login(username='indiv', password='aaa123aaa123')
        response = self.client.get(reverse('register-donatur-yayasan'))

        self.assertRedirects(response, reverse('index'))

    def test_register_donatur_yayasan_post_not_valid(self):
        response = self.client.post(reverse('register-donatur-yayasan'), {
            'username': 'yayasan',
            'password': 'aaa123',  # minimum 8
        })

        self.assertTemplateUsed(response, 'register_donatur_yayasan.html')

    def test_register_donatur_yayasan_post_valid(self):
        response = self.client.post(reverse('register-donatur-yayasan'), {
            'username': 'yayasan',
            'password': 'aaa123aaa123',
            'nomor_identitas': '322322',
            'email': 'mail@mail.com',
            'nama': 'Yayasan Ku',
            'npwp': '12345123451234512345',
            'no_telp': '',
            'alamat': 'Jalan antah berantah',
            'no_sk_yayasan': '007123'
        })

        self.assertRedirects(response, reverse('index'))


