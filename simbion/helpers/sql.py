
def create_insert_command(table_name, **kwargs):
    fields, values = zip(*(kwargs.items()))
    new_values = []
    for value in values:
        if value is not None:
            new_values.append("'" + str(value) + "'")
        else:
            new_values.append('NULL')
    command = 'INSERT INTO ' + table_name + ' (' + ', '.join(fields) + ') VALUES (' + ', '.join(new_values) + ')'

    return command