from ..models import SkemaBeasiswaAktif, Pendaftaran
from datetime import datetime, date
from django.db import connection
from ..helpers import sql

def get_open_skema_aktifs():
    query_set = SkemaBeasiswaAktif.objects.raw("SELECT * FROM " + SkemaBeasiswaAktif._meta.db_table + " WHERE "
                                               + "tgl_mulai_pendaftaran <= %s AND tgl_tutup_pendaftaran >= %s",
                                               [str(date.today()), str(date.today())])

    return list(query_set)

def get_skema_beasiswa_aktif(kode_skema_beasiswa, no_urut):
    query_set = SkemaBeasiswaAktif.objects.raw("SELECT * FROM " + SkemaBeasiswaAktif._meta.db_table + " WHERE "
                                               + "kode_skema_beasiswa = %s AND no_urut = %s",
                                               [kode_skema_beasiswa, no_urut])

    return list(query_set)[0]


def register_beasiswa(mahasiswa,skema_beasiswa_aktif):
    with connection.cursor() as cursor:
        command = sql.create_insert_command(Pendaftaran._meta.db_table,
                                            no_urut=skema_beasiswa_aktif.no_urut,
                                            kode_skema_beasiswa=skema_beasiswa_aktif.kode_skema_beasiswa.kode,
                                            npm=mahasiswa.npm,
                                            waktu_daftar=datetime.now(),
                                            status_daftar='Terdaftar',
                                            status_terima='Pending')
        cursor.execute(command)
