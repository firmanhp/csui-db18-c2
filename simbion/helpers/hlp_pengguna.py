from ..models import Pengguna


def is_username_used(username):
    query_set = Pengguna.objects.raw("SELECT * FROM " + Pengguna._meta.db_table + " WHERE username = %s", [username])
    return len(list(query_set)) > 0