from ..models import IndividualDonor


def is_nik_used(nik):
    query_set = IndividualDonor.objects.raw("SELECT * FROM " + IndividualDonor._meta.db_table
                                            + " WHERE nik = %s", [nik])
    return len(list(query_set)) > 0