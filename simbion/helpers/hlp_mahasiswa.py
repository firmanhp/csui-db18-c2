from ..models import Mahasiswa


def is_npm_used(npm):
    query_set = Mahasiswa.objects.raw("SELECT * FROM " + Mahasiswa._meta.db_table + " WHERE npm = %s", [npm])
    return len(list(query_set)) > 0


def get_user_mahasiswa(user):
    query_set = Mahasiswa.objects.raw("SELECT * FROM " + Mahasiswa._meta.db_table + " WHERE username = %s", [str(user.username)])

    return list(query_set)[0]