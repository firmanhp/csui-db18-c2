import datetime
from ..models import Donatur, SkemaBeasiswa, SyaratBeasiswa, SkemaBeasiswaAktif, Pendaftaran
from django.db import connection
from simbion.helpers import sql


def is_nomor_identitas_used(nomor_identitas):
    query_set = Donatur.objects.raw("SELECT * FROM " + Donatur._meta.db_table + " WHERE nomor_identitas = %s",
                                    [nomor_identitas])
    return len(list(query_set)) > 0


def insert_buat_paket_beasiswa(request, data):
    query_set = Donatur.objects.raw("SELECT * FROM " + Donatur._meta.db_table + " WHERE username = %s", [str(request.user)])
    nomor_identitas = list(query_set)[0].nomor_identitas

    with connection.cursor() as cursor:
        command = sql.create_insert_command(SkemaBeasiswa._meta.db_table,
                                            kode=data['kode'],
                                            nama=data['nama_paket'],
                                            jenis=data['jenis_beasiswa'],
                                            deskripsi=data['deskripsi'],
                                            nomor_identitas_donatur=nomor_identitas)
        cursor.execute(command)
        query_set = SkemaBeasiswa.objects.raw("SELECT * FROM " + SkemaBeasiswa._meta.db_table + " WHERE kode = %s",
                                              [data['kode']])

        if len(list(query_set)) > 0:
            current = 1
            syarat_html_name = 'syarat' + str(current)
            while syarat_html_name in request.POST:
                syarat = request.POST[syarat_html_name]
                command = sql.create_insert_command(SyaratBeasiswa._meta.db_table,
                                                kode_beasiswa=data['kode'],
                                                syarat=syarat)
                cursor.execute(command)
                query_set = SyaratBeasiswa.objects.raw(
                    "SELECT * FROM " + SyaratBeasiswa._meta.db_table + " WHERE kode_beasiswa = %s", [data['kode']])
                if len(list(query_set)) <= 0:
                    raise Exception('Error when creating beasiswa')

                current += 1
                syarat_html_name = 'syarat' + str(current)
        else:
            raise Exception('Error when creating beasiswa')


def insert_tambahkan_paket_beasiswa(username, data):
    with connection.cursor() as cursor:
        status = make_status(data['tanggal_tutup'])
        command = sql.create_insert_command(SkemaBeasiswaAktif._meta.db_table,
                                            kode_skema_beasiswa=data['kode_beasiswa'],
                                            no_urut=data['nomor_urut'],
                                            tgl_mulai_pendaftaran=data['tanggal_mulai'],
                                            tgl_tutup_pendaftaran=data['tanggal_tutup'],
                                            periode_penerimaan='',
                                            status=status,
                                            jumlah_pendaftar=0,
                                            total_pembayaran=0)
        cursor.execute(command)
        query_set = SkemaBeasiswaAktif.objects.raw(
            "SELECT * FROM " + SkemaBeasiswaAktif._meta.db_table + " WHERE kode_skema_beasiswa = %s AND no_urut = %s",
            [data['kode_beasiswa'], data['nomor_urut']])

        if len(list(query_set)) <= 0:
            raise Exception('Error when creating beasiswa')


def make_status(end_date):
    now = datetime.datetime.now()
    now = datetime.date(now.year, now.month, now.day)

    if now < end_date:
        return 'Dibuka'
    return 'Ditutup'


def get_kode_beasiswa(username):
    ret = ['Pilih Kode Beasiswa']

    if not username == None:
        kode_list = []

        query_set = Donatur.objects.raw("SELECT * FROM " + Donatur._meta.db_table + " WHERE username = %s",
                                        [str(username)])
        nomor_identitas = list(query_set)[0].nomor_identitas

        query_set = SkemaBeasiswa.objects.raw(
            "SELECT * FROM " + SkemaBeasiswa._meta.db_table + " WHERE nomor_identitas_donatur = %s", [nomor_identitas])
        for beasiswa in list(query_set):
            kode_list.append(int(beasiswa.kode))
        kode_list.sort()

        ret = ret + kode_list

    return ret

def tolak_mahasiswa(id, no_urut, npm):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE " + Pendaftaran._meta.db_table + " SET"
            + " status_terima = 'Tidak Aktif' WHERE kode_skema_beasiswa = %s AND no_urut = %s AND npm = %s", [id, no_urut, npm])

        query_set = Pendaftaran.objects.raw("SELECT * FROM " + Pendaftaran._meta.db_table 
            + " WHERE kode_skema_beasiswa = %s AND no_urut = %s AND npm = %s", [id, no_urut, npm])
        status = list(query_set)[0].status_terima

        if status != 'Tidak Aktif':
            raise Exception('Error when updating status_terima')

def terima_mahasiswa(id, no_urut, npm):
    with connection.cursor() as cursor:
        cursor.execute("UPDATE " + Pendaftaran._meta.db_table + " SET"
            + " status_terima = 'Aktif' WHERE kode_skema_beasiswa = %s AND no_urut = %s AND npm = %s", [id, no_urut, npm])

        query_set = Pendaftaran.objects.raw("SELECT * FROM " + Pendaftaran._meta.db_table 
            + " WHERE kode_skema_beasiswa = %s AND no_urut = %s AND npm = %s", [id, no_urut, npm])
        status = list(query_set)[0].status_terima

        if status != 'Aktif':
            raise Exception('Error when updating status_terima')