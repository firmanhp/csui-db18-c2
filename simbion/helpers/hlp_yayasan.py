from ..models import Yayasan


def is_no_sk_yayasan_used(no_sk_yayasan):
    query_set = Yayasan.objects.raw("SELECT * FROM " + Yayasan._meta.db_table
                                            + " WHERE no_sk_yayasan = %s", [no_sk_yayasan])
    return len(list(query_set)) > 0