# SIMBION

Kelompok 2 Tugas Kelompok

Basis Data FASILKOM UI kelas C, Semester Genap 2017/2018

Repositori _git_ ini berisi kode sumber untuk aplikasi _SIMBION_,
yang dibuat oleh kelompok 2 yang beranggotakan:

- Kautsar Fadlillah (1606822642)
- Firman Hadi Prayoga (1606862721)
- Fauzan Deni Rywannis (1606881355)
  
  
## Status

Berikut adalah informasi detil tentang _branch_ _master_:

- Situs: _TBA_
- [![pipeline status](https://gitlab.com/firmanhp/csui-db18-c2/badges/master/pipeline.svg)](https://gitlab.com/firmanhp/csui-db18-c2/commits/master)
- [![coverage report](https://gitlab.com/firmanhp/csui-db18-c2/badges/master/coverage.svg)](https://gitlab.com/firmanhp/csui-db18-c2/commits/master)
  
## Catatan Untuk Pengembang (Anggota Kelompok)

### Memulai untuk pertama kalinya

**Cobalah untuk me-_refresh_ pengetahuan Anda tentang _Django_ dari repositori praktikum PPW Anda.**
Versi dari _Django_ yang digunakan sama persis dengan versi yang digunakan pada praktikum (1.11.4). Disediakan juga
_Selenium_ bagi yang ingin melakukan _functional test_.

Anda bisa mulai pekerjaan Anda dengan mengubah isi dari _static, views, urls, templates_ yang ada pada aplikasi
``simbion``. Isi awal dari aplikasi tersebut merupakan contoh untuk memahami struktur proyek yang digunakan.

### Konvensi dalam proyek ini

Konvensi yang sudah jelas seperti penamaan variabel harus rapih dan dapat dimengerti orang lain tidak akan ditulis disini.
Harap kerjasamanya :)

- Seharusnya, Anda tidak perlu membuat aplikasi baru lagi (cukup ``simbion`` saja). Namun apabila terdapat kondisi
yang mengharuskan Anda untuk membuat aplikasi baru (_readability_, _maintainability_, _requirement_, dll) Anda dapat melakukan
diskusi dengan kelompok masing-masing terlebih dahulu.

- **Jangan langsung push ke _branch_ _master_!**
  
  - Buatlah _merge request_ ke _branch_ tersebut dan tunggu teman Anda me-_review_ pekerjaan Anda. Lakukan seperti
  Anda mengerjakan praktikum Pemrograman Lanjut (_Advanced Programming_).
  

- **Konvensi di atas dibuat untuk mempermudah dalam menyelesaikan tahap 1 dari proyek ini.**
Masih banyak hal lain yang harus diperhatikan, namun belum sempat ditulis disini.
Konvensi-konvensi tambahan akan ditulis di dalam grup LINE.

### Saran

- Untuk mempermudah integrasi, sebisa mungkin gunakan format penamaan ``namafitur_namaobjek`` untuk nama _template_,
_method view_, _static file_, dan lain-lain. 

- Bagi yang ingin membuat struktur kode yang rapih bisa gunakan IDE seperti _PyCharm_. IDE dapat menotifikasi _coding style_ yang salah
(tidak memenuhi _PEP 8_ : https://pep8.org/).


### Informasi lebih lanjut

Apabila ada permasalahan dalam _pipeline_ (_error_ dalam _deployment_, bukan _unit test_ yang gagal karena kesalahan pembuat kode),
Anda dapat menghubungi Firman.


_Good luck and have fun!_
-firmanhp