"""simbion URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from simbion.views import index, login, register, create_admin, \
    register_mahasiswa, register_donatur_individu, register_donatur_yayasan, logout, donatur_beasiswa_baru, \
    donatur_beasiswa_baru_membuat_paket, donatur_beasiswa_baru_tambahkan_paket, beasiswa_detail, mahasiswa_beasiswa_detail, \
    donatur_list_beasiswa, donatur_beasiswa, beasiswa_register, donatur_tolak_mahasiswa, donatur_terima_mahasiswa

urlpatterns = [
    url(r'^saya-tampan/', admin.site.urls),
    url(r'^$', index, name='index'),
    url(r'^login/$', login, name='login'),
    url(r'^register/$', register, name='register'),
    url(r'^register/mahasiswa/$', register_mahasiswa, name='register-mhs'),
    url(r'^register/donatur/individu/$', register_donatur_individu, name='register-donatur-individu'),
    url(r'^register/donatur/yayasan/$', register_donatur_yayasan, name='register-donatur-yayasan'),
    url(r'^donatur/beasiswa_baru$', donatur_beasiswa_baru, name='donatur_beasiswa_baru'),
    url(r'^donatur/beasiswa_baru/membuat_paket/$', donatur_beasiswa_baru_membuat_paket, name='donatur_beasiswa_baru_membuat_paket'),
    url(r'^donatur/beasiswa_baru/tambahkan_paket/$', donatur_beasiswa_baru_tambahkan_paket, name='donatur_beasiswa_baru_tambahkan_paket'),
    url(r'^donatur/list_beasiswa/$', donatur_list_beasiswa, name='donatur_list_beasiswa'),
    url(r'^donatur/beasiswa/(?P<id>.*)/(?P<no_urut>.*)/(?P<npm>.*)/tolak/$', donatur_tolak_mahasiswa, name='donatur_tolak_mahasiswa'),
    url(r'^donatur/beasiswa/(?P<id>.*)/(?P<no_urut>.*)/(?P<npm>.*)/terima/$', donatur_terima_mahasiswa, name='donatur_terima_mahasiswa'),
    url(r'^donatur/beasiswa/(?P<id>.*)/(?P<no_urut>.*)/$', donatur_beasiswa, name='donatur_beasiswa'),
    url(r'^mahasiswa/beasiswa/detail/(?P<id>.*)/(?P<no_urut>.*)/$', mahasiswa_beasiswa_detail, name='mahasiswa_beasiswa_detail'),
    url(r'^beasiswa/detail/(?P<id>.*)/$', beasiswa_detail, name='beasiswa_detail'),
    url(r'^beasiswa/register/$', beasiswa_register, name='beasiswa-register'),
    url(r'^logout/$', logout, name='logout'),
    url(r'^create-admin/$', create_admin, name='create-admin')
]
