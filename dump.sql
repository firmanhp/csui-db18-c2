--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.15
-- Dumped by pg_dump version 9.6.7

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: simbion; Type: SCHEMA; Schema: -; Owner: db042
--

CREATE SCHEMA simbion;


ALTER SCHEMA simbion OWNER TO db042;

SET search_path = simbion, pg_catalog;

--
-- Name: upd_jumlah_pendaftar(); Type: FUNCTION; Schema: simbion; Owner: db042
--

CREATE FUNCTION upd_jumlah_pendaftar() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                    DECLARE
                        int_jumlah_pendaftar INTEGER;
                        row RECORD;
                    BEGIN
                        FOR row IN SELECT kode_skema_beasiswa, no_urut
                            FROM SKEMA_BEASISWA_AKTIF
                        LOOP
                            SELECT COUNT(*) INTO int_jumlah_pendaftar
                                FROM PENDAFTARAN
                                WHERE no_urut = row.no_urut
                                AND kode_skema_beasiswa = row.kode_skema_beasiswa;
                
                            UPDATE SKEMA_BEASISWA_AKTIF SET jumlah_pendaftar = int_jumlah_pendaftar
                                WHERE no_urut = row.no_urut
                                AND kode_skema_beasiswa = row.kode_skema_beasiswa;
                        END LOOP;
                
                        RETURN NULL;
                    END;
                $$;


ALTER FUNCTION simbion.upd_jumlah_pendaftar() OWNER TO db042;

--
-- Name: upd_total_pembayaran(); Type: FUNCTION; Schema: simbion; Owner: db042
--

CREATE FUNCTION upd_total_pembayaran() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                DECLARE
                    int_total_pembayaran BIGINT;
                    row RECORD;
                BEGIN
                    FOR row IN SELECT kode_skema_beasiswa, no_urut
                        FROM SKEMA_BEASISWA_AKTIF
                    LOOP
                        SELECT SUM(nominal) INTO int_total_pembayaran
                            FROM PEMBAYARAN
                            WHERE no_urut_skema_beasiswa_aktif = row.no_urut
                            AND kode_skema_beasiswa = row.kode_skema_beasiswa;
            
                        IF (int_total_pembayaran IS NOT NULL) THEN
                            UPDATE SKEMA_BEASISWA_AKTIF SET total_pembayaran = int_total_pembayaran
                                WHERE no_urut = row.no_urut
                                AND kode_skema_beasiswa = row.kode_skema_beasiswa;
                        END IF;
                    END LOOP;
            
                    RETURN NULL;
                END;
            $$;


ALTER FUNCTION simbion.upd_total_pembayaran() OWNER TO db042;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admin; Type: TABLE; Schema: simbion; Owner: db042
--

CREATE TABLE admin (
    username character varying(20) NOT NULL
);


ALTER TABLE admin OWNER TO db042;

--
-- Name: auth_group; Type: TABLE; Schema: simbion; Owner: db042
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE auth_group OWNER TO db042;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: simbion; Owner: db042
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_id_seq OWNER TO db042;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: simbion; Owner: db042
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: simbion; Owner: db042
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_group_permissions OWNER TO db042;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: simbion; Owner: db042
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_permissions_id_seq OWNER TO db042;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: simbion; Owner: db042
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: simbion; Owner: db042
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE auth_permission OWNER TO db042;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: simbion; Owner: db042
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_permission_id_seq OWNER TO db042;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: simbion; Owner: db042
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: simbion; Owner: db042
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id character varying(20) NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE django_admin_log OWNER TO db042;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: simbion; Owner: db042
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_admin_log_id_seq OWNER TO db042;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: simbion; Owner: db042
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: simbion; Owner: db042
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE django_content_type OWNER TO db042;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: simbion; Owner: db042
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_content_type_id_seq OWNER TO db042;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: simbion; Owner: db042
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: simbion; Owner: db042
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE django_migrations OWNER TO db042;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: simbion; Owner: db042
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_migrations_id_seq OWNER TO db042;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: simbion; Owner: db042
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: simbion; Owner: db042
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE django_session OWNER TO db042;

--
-- Name: donatur; Type: TABLE; Schema: simbion; Owner: db042
--

CREATE TABLE donatur (
    nomor_identitas character varying(20) NOT NULL,
    email character varying(50) NOT NULL,
    nama character varying(50) NOT NULL,
    npwp character(20) NOT NULL,
    no_telp character varying(20),
    alamat character varying(50) NOT NULL,
    username character varying(20) NOT NULL
);


ALTER TABLE donatur OWNER TO db042;

--
-- Name: individual_donor; Type: TABLE; Schema: simbion; Owner: db042
--

CREATE TABLE individual_donor (
    nik character(16) NOT NULL,
    nomor_identitas_donatur character varying(20) NOT NULL
);


ALTER TABLE individual_donor OWNER TO db042;

--
-- Name: mahasiswa; Type: TABLE; Schema: simbion; Owner: db042
--

CREATE TABLE mahasiswa (
    npm character varying(20) NOT NULL,
    email character varying(50) NOT NULL,
    nama character varying(50) NOT NULL,
    no_telp character varying(20),
    alamat_tinggal character varying(50) NOT NULL,
    alamat_domisili character varying(50) NOT NULL,
    nama_bank character varying(50) NOT NULL,
    no_rekening character varying(20) NOT NULL,
    nama_pemilik character varying(20) NOT NULL,
    username character varying(20) NOT NULL
);


ALTER TABLE mahasiswa OWNER TO db042;

--
-- Name: pembayaran; Type: TABLE; Schema: simbion; Owner: db042
--

CREATE TABLE pembayaran (
    urutan integer NOT NULL,
    kode_skema_beasiswa integer NOT NULL,
    no_urut_skema_beasiswa_aktif integer NOT NULL,
    npm character varying(20) NOT NULL,
    keterangan character varying(50) NOT NULL,
    tgl_bayar date NOT NULL,
    nominal integer NOT NULL
);


ALTER TABLE pembayaran OWNER TO db042;

--
-- Name: pendaftaran; Type: TABLE; Schema: simbion; Owner: db042
--

CREATE TABLE pendaftaran (
    no_urut integer NOT NULL,
    kode_skema_beasiswa integer NOT NULL,
    npm character varying(20) NOT NULL,
    waktu_daftar timestamp without time zone NOT NULL,
    status_daftar character varying(20) NOT NULL,
    status_terima character varying(20) NOT NULL
);


ALTER TABLE pendaftaran OWNER TO db042;

--
-- Name: pengguna; Type: TABLE; Schema: simbion; Owner: db042
--

CREATE TABLE pengguna (
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    username character varying(20) NOT NULL,
    role character varying(20) NOT NULL,
    is_superuser boolean NOT NULL
);


ALTER TABLE pengguna OWNER TO db042;

--
-- Name: pengguna_groups; Type: TABLE; Schema: simbion; Owner: db042
--

CREATE TABLE pengguna_groups (
    id integer NOT NULL,
    pengguna_id character varying(20) NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE pengguna_groups OWNER TO db042;

--
-- Name: pengguna_groups_id_seq; Type: SEQUENCE; Schema: simbion; Owner: db042
--

CREATE SEQUENCE pengguna_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pengguna_groups_id_seq OWNER TO db042;

--
-- Name: pengguna_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: simbion; Owner: db042
--

ALTER SEQUENCE pengguna_groups_id_seq OWNED BY pengguna_groups.id;


--
-- Name: pengguna_user_permissions; Type: TABLE; Schema: simbion; Owner: db042
--

CREATE TABLE pengguna_user_permissions (
    id integer NOT NULL,
    pengguna_id character varying(20) NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE pengguna_user_permissions OWNER TO db042;

--
-- Name: pengguna_user_permissions_id_seq; Type: SEQUENCE; Schema: simbion; Owner: db042
--

CREATE SEQUENCE pengguna_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pengguna_user_permissions_id_seq OWNER TO db042;

--
-- Name: pengguna_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: simbion; Owner: db042
--

ALTER SEQUENCE pengguna_user_permissions_id_seq OWNED BY pengguna_user_permissions.id;


--
-- Name: pengumuman; Type: TABLE; Schema: simbion; Owner: db042
--

CREATE TABLE pengumuman (
    tanggal date NOT NULL,
    no_urut_skema_beasiswa_aktif integer NOT NULL,
    kode_skema_beasiswa integer NOT NULL,
    username character varying(20) NOT NULL,
    judul character varying(20) NOT NULL,
    isi character varying(255) NOT NULL
);


ALTER TABLE pengumuman OWNER TO db042;

--
-- Name: riwayat_akademik; Type: TABLE; Schema: simbion; Owner: db042
--

CREATE TABLE riwayat_akademik (
    no_urut integer NOT NULL,
    npm character varying(20) NOT NULL,
    semester character(1) NOT NULL,
    tahun_ajaran character(9) NOT NULL,
    jumlah_sks integer NOT NULL,
    ips double precision NOT NULL,
    lampiran character varying(50) NOT NULL
);


ALTER TABLE riwayat_akademik OWNER TO db042;

--
-- Name: skema_beasiswa; Type: TABLE; Schema: simbion; Owner: db042
--

CREATE TABLE skema_beasiswa (
    kode integer NOT NULL,
    nama character varying(50) NOT NULL,
    jenis character varying(20) NOT NULL,
    deskripsi character varying(50) NOT NULL,
    nomor_identitas_donatur character varying(20) NOT NULL
);


ALTER TABLE skema_beasiswa OWNER TO db042;

--
-- Name: skema_beasiswa_aktif; Type: TABLE; Schema: simbion; Owner: db042
--

CREATE TABLE skema_beasiswa_aktif (
    kode_skema_beasiswa integer NOT NULL,
    no_urut integer NOT NULL,
    tgl_mulai_pendaftaran date NOT NULL,
    tgl_tutup_pendaftaran date NOT NULL,
    periode_penerimaan character varying(50) NOT NULL,
    status character varying(20) NOT NULL,
    jumlah_pendaftar integer NOT NULL,
    total_pembayaran bigint NOT NULL
);


ALTER TABLE skema_beasiswa_aktif OWNER TO db042;

--
-- Name: syarat_beasiswa; Type: TABLE; Schema: simbion; Owner: db042
--

CREATE TABLE syarat_beasiswa (
    kode_beasiswa integer NOT NULL,
    syarat character varying(50) NOT NULL
);


ALTER TABLE syarat_beasiswa OWNER TO db042;

--
-- Name: tempat_wawancara; Type: TABLE; Schema: simbion; Owner: db042
--

CREATE TABLE tempat_wawancara (
    kode integer NOT NULL,
    nama character varying(50) NOT NULL,
    lokasi character varying(50) NOT NULL
);


ALTER TABLE tempat_wawancara OWNER TO db042;

--
-- Name: wawancara; Type: TABLE; Schema: simbion; Owner: db042
--

CREATE TABLE wawancara (
    no_urut_skema_beasiswa_aktif integer NOT NULL,
    kode_skema_beasiswa integer NOT NULL,
    jadwal timestamp without time zone NOT NULL,
    kode_tempat_wawancara integer NOT NULL
);


ALTER TABLE wawancara OWNER TO db042;

--
-- Name: yayasan; Type: TABLE; Schema: simbion; Owner: db042
--

CREATE TABLE yayasan (
    no_sk_yayasan character varying(20) NOT NULL,
    email character varying(50) NOT NULL,
    nama character varying(50) NOT NULL,
    no_telp_cp character varying(20),
    nomor_identitas_donatur character varying(20) NOT NULL
);


ALTER TABLE yayasan OWNER TO db042;

--
-- Name: auth_group id; Type: DEFAULT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- Name: pengguna_groups id; Type: DEFAULT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY pengguna_groups ALTER COLUMN id SET DEFAULT nextval('pengguna_groups_id_seq'::regclass);


--
-- Name: pengguna_user_permissions id; Type: DEFAULT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY pengguna_user_permissions ALTER COLUMN id SET DEFAULT nextval('pengguna_user_permissions_id_seq'::regclass);


--
-- Data for Name: admin; Type: TABLE DATA; Schema: simbion; Owner: db042
--

COPY admin (username) FROM stdin;
admin
\.


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: simbion; Owner: db042
--

COPY auth_group (id, name) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: simbion; Owner: db042
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, false);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: simbion; Owner: db042
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: simbion; Owner: db042
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: simbion; Owner: db042
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add permission	2	add_permission
5	Can change permission	2	change_permission
6	Can delete permission	2	delete_permission
7	Can add group	3	add_group
8	Can change group	3	change_group
9	Can delete group	3	delete_group
10	Can add content type	4	add_contenttype
11	Can change content type	4	change_contenttype
12	Can delete content type	4	delete_contenttype
13	Can add session	5	add_session
14	Can change session	5	change_session
15	Can delete session	5	delete_session
16	Can add admin	6	add_admin
17	Can change admin	6	change_admin
18	Can delete admin	6	delete_admin
19	Can add skema beasiswa	7	add_skemabeasiswa
20	Can change skema beasiswa	7	change_skemabeasiswa
21	Can delete skema beasiswa	7	delete_skemabeasiswa
22	Can add pengguna	8	add_pengguna
23	Can change pengguna	8	change_pengguna
24	Can delete pengguna	8	delete_pengguna
25	Can add mahasiswa	9	add_mahasiswa
26	Can change mahasiswa	9	change_mahasiswa
27	Can delete mahasiswa	9	delete_mahasiswa
28	Can add syarat beasiswa	10	add_syaratbeasiswa
29	Can change syarat beasiswa	10	change_syaratbeasiswa
30	Can delete syarat beasiswa	10	delete_syaratbeasiswa
31	Can add yayasan	11	add_yayasan
32	Can change yayasan	11	change_yayasan
33	Can delete yayasan	11	delete_yayasan
34	Can add riwayat akademik	12	add_riwayatakademik
35	Can change riwayat akademik	12	change_riwayatakademik
36	Can delete riwayat akademik	12	delete_riwayatakademik
37	Can add wawancara	13	add_wawancara
38	Can change wawancara	13	change_wawancara
39	Can delete wawancara	13	delete_wawancara
40	Can add pembayaran	14	add_pembayaran
41	Can change pembayaran	14	change_pembayaran
42	Can delete pembayaran	14	delete_pembayaran
43	Can add skema beasiswa aktif	15	add_skemabeasiswaaktif
44	Can change skema beasiswa aktif	15	change_skemabeasiswaaktif
45	Can delete skema beasiswa aktif	15	delete_skemabeasiswaaktif
46	Can add pendaftaran	16	add_pendaftaran
47	Can change pendaftaran	16	change_pendaftaran
48	Can delete pendaftaran	16	delete_pendaftaran
49	Can add donatur	17	add_donatur
50	Can change donatur	17	change_donatur
51	Can delete donatur	17	delete_donatur
52	Can add individual donor	18	add_individualdonor
53	Can change individual donor	18	change_individualdonor
54	Can delete individual donor	18	delete_individualdonor
55	Can add tempat wawancara	19	add_tempatwawancara
56	Can change tempat wawancara	19	change_tempatwawancara
57	Can delete tempat wawancara	19	delete_tempatwawancara
58	Can add pengumuman	20	add_pengumuman
59	Can change pengumuman	20	change_pengumuman
60	Can delete pengumuman	20	delete_pengumuman
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: simbion; Owner: db042
--

SELECT pg_catalog.setval('auth_permission_id_seq', 60, true);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: simbion; Owner: db042
--

COPY django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: simbion; Owner: db042
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 1, false);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: simbion; Owner: db042
--

COPY django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	contenttypes	contenttype
5	sessions	session
6	simbion	admin
7	simbion	skemabeasiswa
8	simbion	pengguna
9	simbion	mahasiswa
10	simbion	syaratbeasiswa
11	simbion	yayasan
12	simbion	riwayatakademik
13	simbion	wawancara
14	simbion	pembayaran
15	simbion	skemabeasiswaaktif
16	simbion	pendaftaran
17	simbion	donatur
18	simbion	individualdonor
19	simbion	tempatwawancara
20	simbion	pengumuman
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: simbion; Owner: db042
--

SELECT pg_catalog.setval('django_content_type_id_seq', 20, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: simbion; Owner: db042
--

COPY django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2018-05-21 11:32:22.329268+07
2	contenttypes	0002_remove_content_type_name	2018-05-21 11:32:22.370711+07
3	auth	0001_initial	2018-05-21 11:32:22.982552+07
4	auth	0002_alter_permission_name_max_length	2018-05-21 11:32:23.0477+07
5	auth	0003_alter_user_email_max_length	2018-05-21 11:32:23.071092+07
6	auth	0004_alter_user_username_opts	2018-05-21 11:32:23.089262+07
7	auth	0005_alter_user_last_login_null	2018-05-21 11:32:23.106559+07
8	auth	0006_require_contenttypes_0002	2018-05-21 11:32:23.119498+07
9	auth	0007_alter_validators_add_error_messages	2018-05-21 11:32:23.136783+07
10	auth	0008_alter_user_username_max_length	2018-05-21 11:32:23.161684+07
11	simbion	0001_initial	2018-05-21 11:32:24.446442+07
12	admin	0001_initial	2018-05-21 11:32:24.770466+07
13	admin	0002_logentry_remove_auto_add	2018-05-21 11:32:24.828624+07
14	sessions	0001_initial	2018-05-21 11:32:25.087481+07
15	simbion	0002_triggers	2018-05-21 11:32:25.129047+07
\.


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: simbion; Owner: db042
--

SELECT pg_catalog.setval('django_migrations_id_seq', 15, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: simbion; Owner: db042
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
\.


--
-- Data for Name: donatur; Type: TABLE DATA; Schema: simbion; Owner: db042
--

COPY donatur (nomor_identitas, email, nama, npwp, no_telp, alamat, username) FROM stdin;
123456	yayasan@ui.ac.id	Yayasan Universitas Indonesia	12345123451234512345		salkdjalfkjsdaf	yayasan
12341234	individu@ui.ac.id	Donatur Individu Universitas Indonesia	12341234123412341234	021123123123	Jl. Pegangsaan Timur No.56, Pegangsaan, Menteng,	individu
\.


--
-- Data for Name: individual_donor; Type: TABLE DATA; Schema: simbion; Owner: db042
--

COPY individual_donor (nik, nomor_identitas_donatur) FROM stdin;
1234123412341234	12341234
\.


--
-- Data for Name: mahasiswa; Type: TABLE DATA; Schema: simbion; Owner: db042
--

COPY mahasiswa (npm, email, nama, no_telp, alamat_tinggal, alamat_domisili, nama_bank, no_rekening, nama_pemilik, username) FROM stdin;
1606862721	mahasiswa@ui.ac.id	Mahasiswa Universitas Indonesia	218943214	dlkfjdsalkfjskdf	asfkljdsaflksaf	BNI	021123123	Mahasiswa	mahasiswa
1606862722	mahasiswa@ui.ac.id	Mahasiswa Universitas Indonesia	0815123123	Jl. Pegangsaan Timur No.56, Pegangsaan, Menteng,	Jl. Pegangsaan Timur No.56, Pegangsaan, Menteng,	BNI	123123123	Mahasiswa	mhs
\.


--
-- Data for Name: pembayaran; Type: TABLE DATA; Schema: simbion; Owner: db042
--

COPY pembayaran (urutan, kode_skema_beasiswa, no_urut_skema_beasiswa_aktif, npm, keterangan, tgl_bayar, nominal) FROM stdin;
\.


--
-- Data for Name: pendaftaran; Type: TABLE DATA; Schema: simbion; Owner: db042
--

COPY pendaftaran (no_urut, kode_skema_beasiswa, npm, waktu_daftar, status_daftar, status_terima) FROM stdin;
1	1	1606862722	2018-05-21 12:04:01.270777	Terdaftar	Aktif
1	2	1606862722	2018-05-21 12:05:46.445484	Terdaftar	Tidak Aktif
\.


--
-- Data for Name: pengguna; Type: TABLE DATA; Schema: simbion; Owner: db042
--

COPY pengguna (password, last_login, username, role, is_superuser) FROM stdin;
pbkdf2_sha256$36000$DPIMIRS68rqP$tsN80kjXR7mhFGL4jyfT4jTG9tOKfeIes0qHubnT91Y=	2018-05-21 11:42:51.286395+07	mahasiswa	mahasiswa	f
pbkdf2_sha256$36000$Doe4Y4zZWUQO$DbToYwafh4fBThl/lkyEeCZ07O7apTlZgVpL2kGQxOY=	2018-05-21 11:53:57.217419+07	individu	donatur	f
pbkdf2_sha256$36000$k0JJxXG6PjKv$V6Fxpbk+V30tOP71Zy47z+y6WGfFx9o+RqSR9mbzAX0=	2018-05-21 12:05:36.862029+07	mhs	mahasiswa	f
pbkdf2_sha256$36000$xwsUMb1XvZ1y$XnyaqTzRrgxNSwbbWispY7yQBrlYpAvJPWuEbfjsokc=	2018-05-21 12:05:53.457899+07	yayasan	donatur	f
pbkdf2_sha256$36000$2ZJbZhiYxbjX$49dLGDpzPihafVovlVgWxdACFw7R88cgSIQtGDttNS4=	2018-05-21 13:09:28.91314+07	admin	admin	t
\.


--
-- Data for Name: pengguna_groups; Type: TABLE DATA; Schema: simbion; Owner: db042
--

COPY pengguna_groups (id, pengguna_id, group_id) FROM stdin;
\.


--
-- Name: pengguna_groups_id_seq; Type: SEQUENCE SET; Schema: simbion; Owner: db042
--

SELECT pg_catalog.setval('pengguna_groups_id_seq', 1, false);


--
-- Data for Name: pengguna_user_permissions; Type: TABLE DATA; Schema: simbion; Owner: db042
--

COPY pengguna_user_permissions (id, pengguna_id, permission_id) FROM stdin;
\.


--
-- Name: pengguna_user_permissions_id_seq; Type: SEQUENCE SET; Schema: simbion; Owner: db042
--

SELECT pg_catalog.setval('pengguna_user_permissions_id_seq', 1, false);


--
-- Data for Name: pengumuman; Type: TABLE DATA; Schema: simbion; Owner: db042
--

COPY pengumuman (tanggal, no_urut_skema_beasiswa_aktif, kode_skema_beasiswa, username, judul, isi) FROM stdin;
\.


--
-- Data for Name: riwayat_akademik; Type: TABLE DATA; Schema: simbion; Owner: db042
--

COPY riwayat_akademik (no_urut, npm, semester, tahun_ajaran, jumlah_sks, ips, lampiran) FROM stdin;
\.


--
-- Data for Name: skema_beasiswa; Type: TABLE DATA; Schema: simbion; Owner: db042
--

COPY skema_beasiswa (kode, nama, jenis, deskripsi, nomor_identitas_donatur) FROM stdin;
1	Beasiswa PPA ABC	Penuh	Beasiswa ini beasiswa test	123456
2	Beasiswa PPA CDE	Parsial	Beasiswa Parsial Dummy	123456
12	Beasiswa EFG	Parsial	Sebuah beasiswa aneh	12341234
\.


--
-- Data for Name: skema_beasiswa_aktif; Type: TABLE DATA; Schema: simbion; Owner: db042
--

COPY skema_beasiswa_aktif (kode_skema_beasiswa, no_urut, tgl_mulai_pendaftaran, tgl_tutup_pendaftaran, periode_penerimaan, status, jumlah_pendaftar, total_pembayaran) FROM stdin;
1	1	2015-02-05	2019-02-05		Dibuka	1	0
1	2	2015-02-05	2018-02-05		Ditutup	0	0
1	3	2019-02-09	2020-02-09		Dibuka	0	0
2	1	2010-02-01	2020-02-01		Dibuka	1	0
12	123	2018-05-21	2020-05-21		Dibuka	0	0
\.


--
-- Data for Name: syarat_beasiswa; Type: TABLE DATA; Schema: simbion; Owner: db042
--

COPY syarat_beasiswa (kode_beasiswa, syarat) FROM stdin;
1	Harus berumur 18 tahun keatas
1	Harus ganteng
2	Harus berumur 18 tahun keatas
2	Tidak boleh jelek
2	Boleh jelek sedikit
12	Tidak boleh aneh
\.


--
-- Data for Name: tempat_wawancara; Type: TABLE DATA; Schema: simbion; Owner: db042
--

COPY tempat_wawancara (kode, nama, lokasi) FROM stdin;
\.


--
-- Data for Name: wawancara; Type: TABLE DATA; Schema: simbion; Owner: db042
--

COPY wawancara (no_urut_skema_beasiswa_aktif, kode_skema_beasiswa, jadwal, kode_tempat_wawancara) FROM stdin;
\.


--
-- Data for Name: yayasan; Type: TABLE DATA; Schema: simbion; Owner: db042
--

COPY yayasan (no_sk_yayasan, email, nama, no_telp_cp, nomor_identitas_donatur) FROM stdin;
1234123412341234	yayasan@ui.ac.id	Yayasan Universitas Indonesia		123456
\.


--
-- Name: admin admin_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY admin
    ADD CONSTRAINT admin_pkey PRIMARY KEY (username);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: donatur donatur_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY donatur
    ADD CONSTRAINT donatur_pkey PRIMARY KEY (nomor_identitas);


--
-- Name: individual_donor individual_donor_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY individual_donor
    ADD CONSTRAINT individual_donor_pkey PRIMARY KEY (nik);


--
-- Name: mahasiswa mahasiswa_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY mahasiswa
    ADD CONSTRAINT mahasiswa_pkey PRIMARY KEY (npm);


--
-- Name: pembayaran pembayaran_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY pembayaran
    ADD CONSTRAINT pembayaran_pkey PRIMARY KEY (urutan, kode_skema_beasiswa, no_urut_skema_beasiswa_aktif, npm);


--
-- Name: pendaftaran pendaftaran_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY pendaftaran
    ADD CONSTRAINT pendaftaran_pkey PRIMARY KEY (no_urut, kode_skema_beasiswa, npm);


--
-- Name: pengguna_groups pengguna_groups_pengguna_id_group_id_5a0eb786_uniq; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY pengguna_groups
    ADD CONSTRAINT pengguna_groups_pengguna_id_group_id_5a0eb786_uniq UNIQUE (pengguna_id, group_id);


--
-- Name: pengguna_groups pengguna_groups_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY pengguna_groups
    ADD CONSTRAINT pengguna_groups_pkey PRIMARY KEY (id);


--
-- Name: pengguna pengguna_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY pengguna
    ADD CONSTRAINT pengguna_pkey PRIMARY KEY (username);


--
-- Name: pengguna_user_permissions pengguna_user_permission_pengguna_id_permission_i_dc0d6306_uniq; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY pengguna_user_permissions
    ADD CONSTRAINT pengguna_user_permission_pengguna_id_permission_i_dc0d6306_uniq UNIQUE (pengguna_id, permission_id);


--
-- Name: pengguna_user_permissions pengguna_user_permissions_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY pengguna_user_permissions
    ADD CONSTRAINT pengguna_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: pengumuman pengumuman_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY pengumuman
    ADD CONSTRAINT pengumuman_pkey PRIMARY KEY (tanggal, no_urut_skema_beasiswa_aktif, kode_skema_beasiswa, username);


--
-- Name: riwayat_akademik riwayat_akademik_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY riwayat_akademik
    ADD CONSTRAINT riwayat_akademik_pkey PRIMARY KEY (no_urut, npm);


--
-- Name: skema_beasiswa_aktif skema_beasiswa_aktif_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY skema_beasiswa_aktif
    ADD CONSTRAINT skema_beasiswa_aktif_pkey PRIMARY KEY (kode_skema_beasiswa, no_urut);


--
-- Name: skema_beasiswa skema_beasiswa_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY skema_beasiswa
    ADD CONSTRAINT skema_beasiswa_pkey PRIMARY KEY (kode);


--
-- Name: syarat_beasiswa syarat_beasiswa_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY syarat_beasiswa
    ADD CONSTRAINT syarat_beasiswa_pkey PRIMARY KEY (kode_beasiswa, syarat);


--
-- Name: tempat_wawancara tempat_wawancara_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY tempat_wawancara
    ADD CONSTRAINT tempat_wawancara_pkey PRIMARY KEY (kode);


--
-- Name: wawancara wawancara_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY wawancara
    ADD CONSTRAINT wawancara_pkey PRIMARY KEY (no_urut_skema_beasiswa_aktif, kode_skema_beasiswa, jadwal);


--
-- Name: yayasan yayasan_pkey; Type: CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY yayasan
    ADD CONSTRAINT yayasan_pkey PRIMARY KEY (no_sk_yayasan);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: simbion; Owner: db042
--

CREATE INDEX auth_group_name_a6ea08ec_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: simbion; Owner: db042
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: simbion; Owner: db042
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: simbion; Owner: db042
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON auth_permission USING btree (content_type_id);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: simbion; Owner: db042
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: simbion; Owner: db042
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON django_admin_log USING btree (user_id);


--
-- Name: django_admin_log_user_id_c564eba6_like; Type: INDEX; Schema: simbion; Owner: db042
--

CREATE INDEX django_admin_log_user_id_c564eba6_like ON django_admin_log USING btree (user_id varchar_pattern_ops);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: simbion; Owner: db042
--

CREATE INDEX django_session_expire_date_a5c62663 ON django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: simbion; Owner: db042
--

CREATE INDEX django_session_session_key_c0390e0f_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: pengguna_groups_group_id_196136cc; Type: INDEX; Schema: simbion; Owner: db042
--

CREATE INDEX pengguna_groups_group_id_196136cc ON pengguna_groups USING btree (group_id);


--
-- Name: pengguna_groups_pengguna_id_1de9971b; Type: INDEX; Schema: simbion; Owner: db042
--

CREATE INDEX pengguna_groups_pengguna_id_1de9971b ON pengguna_groups USING btree (pengguna_id);


--
-- Name: pengguna_groups_pengguna_id_1de9971b_like; Type: INDEX; Schema: simbion; Owner: db042
--

CREATE INDEX pengguna_groups_pengguna_id_1de9971b_like ON pengguna_groups USING btree (pengguna_id varchar_pattern_ops);


--
-- Name: pengguna_user_permissions_pengguna_id_fbce1347; Type: INDEX; Schema: simbion; Owner: db042
--

CREATE INDEX pengguna_user_permissions_pengguna_id_fbce1347 ON pengguna_user_permissions USING btree (pengguna_id);


--
-- Name: pengguna_user_permissions_pengguna_id_fbce1347_like; Type: INDEX; Schema: simbion; Owner: db042
--

CREATE INDEX pengguna_user_permissions_pengguna_id_fbce1347_like ON pengguna_user_permissions USING btree (pengguna_id varchar_pattern_ops);


--
-- Name: pengguna_user_permissions_permission_id_261b39ea; Type: INDEX; Schema: simbion; Owner: db042
--

CREATE INDEX pengguna_user_permissions_permission_id_261b39ea ON pengguna_user_permissions USING btree (permission_id);


--
-- Name: pengguna_username_d4bf60bc_like; Type: INDEX; Schema: simbion; Owner: db042
--

CREATE INDEX pengguna_username_d4bf60bc_like ON pengguna USING btree (username varchar_pattern_ops);


--
-- Name: pendaftaran trg_upd_jumlah_pendaftar; Type: TRIGGER; Schema: simbion; Owner: db042
--

CREATE TRIGGER trg_upd_jumlah_pendaftar AFTER INSERT OR DELETE OR UPDATE ON pendaftaran FOR EACH STATEMENT EXECUTE PROCEDURE upd_jumlah_pendaftar();


--
-- Name: pembayaran trg_upd_total_pembayaran; Type: TRIGGER; Schema: simbion; Owner: db042
--

CREATE TRIGGER trg_upd_total_pembayaran AFTER INSERT OR DELETE OR UPDATE ON pembayaran FOR EACH STATEMENT EXECUTE PROCEDURE upd_total_pembayaran();


--
-- Name: admin admin_username_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY admin
    ADD CONSTRAINT admin_username_fkey FOREIGN KEY (username) REFERENCES pengguna(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_pengguna_username; Type: FK CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_pengguna_username FOREIGN KEY (user_id) REFERENCES pengguna(username) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: donatur donatur_username_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY donatur
    ADD CONSTRAINT donatur_username_fkey FOREIGN KEY (username) REFERENCES pengguna(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: individual_donor individual_donor_nomor_identitas_donatur_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY individual_donor
    ADD CONSTRAINT individual_donor_nomor_identitas_donatur_fkey FOREIGN KEY (nomor_identitas_donatur) REFERENCES donatur(nomor_identitas) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: mahasiswa mahasiswa_username_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY mahasiswa
    ADD CONSTRAINT mahasiswa_username_fkey FOREIGN KEY (username) REFERENCES pengguna(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pembayaran pembayaran_kode_skema_beasiswa_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY pembayaran
    ADD CONSTRAINT pembayaran_kode_skema_beasiswa_fkey FOREIGN KEY (kode_skema_beasiswa, no_urut_skema_beasiswa_aktif) REFERENCES skema_beasiswa_aktif(kode_skema_beasiswa, no_urut) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pembayaran pembayaran_npm_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY pembayaran
    ADD CONSTRAINT pembayaran_npm_fkey FOREIGN KEY (npm) REFERENCES mahasiswa(npm) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pendaftaran pendaftaran_no_urut_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY pendaftaran
    ADD CONSTRAINT pendaftaran_no_urut_fkey FOREIGN KEY (no_urut, kode_skema_beasiswa) REFERENCES skema_beasiswa_aktif(no_urut, kode_skema_beasiswa) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pendaftaran pendaftaran_npm_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY pendaftaran
    ADD CONSTRAINT pendaftaran_npm_fkey FOREIGN KEY (npm) REFERENCES mahasiswa(npm) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengguna_groups pengguna_groups_group_id_196136cc_fk_auth_group_id; Type: FK CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY pengguna_groups
    ADD CONSTRAINT pengguna_groups_group_id_196136cc_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pengguna_groups pengguna_groups_pengguna_id_1de9971b_fk_pengguna_username; Type: FK CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY pengguna_groups
    ADD CONSTRAINT pengguna_groups_pengguna_id_1de9971b_fk_pengguna_username FOREIGN KEY (pengguna_id) REFERENCES pengguna(username) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pengguna_user_permissions pengguna_user_permis_pengguna_id_fbce1347_fk_pengguna_; Type: FK CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY pengguna_user_permissions
    ADD CONSTRAINT pengguna_user_permis_pengguna_id_fbce1347_fk_pengguna_ FOREIGN KEY (pengguna_id) REFERENCES pengguna(username) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pengguna_user_permissions pengguna_user_permis_permission_id_261b39ea_fk_auth_perm; Type: FK CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY pengguna_user_permissions
    ADD CONSTRAINT pengguna_user_permis_permission_id_261b39ea_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pengumuman pengumuman_no_urut_skema_beasiswa_aktif_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY pengumuman
    ADD CONSTRAINT pengumuman_no_urut_skema_beasiswa_aktif_fkey FOREIGN KEY (no_urut_skema_beasiswa_aktif, kode_skema_beasiswa) REFERENCES skema_beasiswa_aktif(no_urut, kode_skema_beasiswa) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengumuman pengumuman_username_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY pengumuman
    ADD CONSTRAINT pengumuman_username_fkey FOREIGN KEY (username) REFERENCES pengguna(username) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: riwayat_akademik riwayat_akademik_npm_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY riwayat_akademik
    ADD CONSTRAINT riwayat_akademik_npm_fkey FOREIGN KEY (npm) REFERENCES mahasiswa(npm) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: skema_beasiswa_aktif skema_beasiswa_aktif_kode_skema_beasiswa_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY skema_beasiswa_aktif
    ADD CONSTRAINT skema_beasiswa_aktif_kode_skema_beasiswa_fkey FOREIGN KEY (kode_skema_beasiswa) REFERENCES skema_beasiswa(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: skema_beasiswa skema_beasiswa_nomor_identitas_donatur_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY skema_beasiswa
    ADD CONSTRAINT skema_beasiswa_nomor_identitas_donatur_fkey FOREIGN KEY (nomor_identitas_donatur) REFERENCES donatur(nomor_identitas) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: syarat_beasiswa syarat_beasiswa_kode_beasiswa_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY syarat_beasiswa
    ADD CONSTRAINT syarat_beasiswa_kode_beasiswa_fkey FOREIGN KEY (kode_beasiswa) REFERENCES skema_beasiswa(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: wawancara wawancara_kode_skema_beasiswa_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY wawancara
    ADD CONSTRAINT wawancara_kode_skema_beasiswa_fkey FOREIGN KEY (kode_skema_beasiswa, no_urut_skema_beasiswa_aktif) REFERENCES skema_beasiswa_aktif(kode_skema_beasiswa, no_urut) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: wawancara wawancara_kode_tempat_wawancara_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY wawancara
    ADD CONSTRAINT wawancara_kode_tempat_wawancara_fkey FOREIGN KEY (kode_tempat_wawancara) REFERENCES tempat_wawancara(kode) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: yayasan yayasan_nomor_identitas_donatur_fkey; Type: FK CONSTRAINT; Schema: simbion; Owner: db042
--

ALTER TABLE ONLY yayasan
    ADD CONSTRAINT yayasan_nomor_identitas_donatur_fkey FOREIGN KEY (nomor_identitas_donatur) REFERENCES donatur(nomor_identitas) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

